/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje třídu NewGameJFRame a třídu NewGameListener, jež je v ní vnořená, viz dokumentační komentáře téchto tříd 
 */

package ija.gui;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Random;
import ija.game.*;
import java.util.*;
import javax.swing.event.*;

/**
 * Třída NewGameJFRame představuje okno pro vytvoření nové hry
 * @author Jan Herec, Pavel Juhaňák
 */
public class NewGameJFRame extends JFrame {

    /** komponenty okna pro nastavení požadovanách vlastností nové hry */
    private JComboBox box_players;
    private JComboBox box_stones;
    private JRadioButton twelve_treasures;
    private JRadioButton twenty_four_treasures;
    private JButton confirm_button;
    
    /** vlastnosti nové hry */
    private static int number_of_players;
    private static int size_of_board;
    private static int number_of_treasures;
    
    /** referenční (bázová) velikost okna, podle které se relativně nastavují velikosti okna nové hry */
    private static int reference_window_width = 1088;
    private static int reference_window_height = 680;

    /**
     * Konstruktor, který zajistí vykreslení komponent a navázání událostí na komponenty
     * @param saveGameAction položka menu sloužící pro uložení hry se po vytvoření hry aktivuje, do té doby je neaktivní 
     */
    public NewGameJFRame(final JMenuItem saveGameAction) {
        
        // výchozí vlastnosti nové hry
        NewGameJFRame.number_of_players = 2; 
        NewGameJFRame.size_of_board = 7;
        NewGameJFRame.number_of_treasures = 12;
        
        this.setTitle("Vytvoření nové hry");
        
        // vytvoříme instanci třídy pro obsluhu událostí
        NewGameListener new_game_listener = new NewGameListener();
        
        // hlavní JPanel tohoto okna
        JPanel panel = new JPanel();
        panel.setPreferredSize(new Dimension(450, 570));
        
        // JPanel který bude obalovat volby pro vytvoření nové hry
        JPanel settings_item = new JPanel();
        settings_item.setLayout(new BoxLayout(settings_item, BoxLayout.PAGE_AXIS));
        
        // JPanel který bude obalovat volby pro vytvoření nové hry, kromě volby velikosti okna
        JPanel panel_grid = new JPanel();
        panel_grid.setLayout(new GridLayout(12, 1, 10, 10));
                
        // přidáme mezeru
        
        panel_grid.add(new JLabel(" "));
        
        // přidáme volbu na počet hráčů
        
        String[] num_of_players = new String[]{"2 Hráči", "3 Hráči", "4 Hráči"};

        this.box_players = new JComboBox<>(num_of_players);
        
        this.box_players.addActionListener(new_game_listener);
        
        JLabel num_of_players_title = new JLabel("Vyberte počet hráčů:");
        
        panel_grid.add(num_of_players_title);
        panel_grid.add(this.box_players);
        
        // přidáme oddělovač
        
        panel_grid.add(new JLabel("__________________________________________"));
        
        // přidáme volbu na velikost hrací desky
        
        String[] num_of_stones = new String[]{"5 x 5", "7 x 7", "9 x 9", "11 x 11"};

        this.box_stones = new JComboBox<>(num_of_stones);
        this.box_stones.setSelectedIndex(1);
        
        this.box_stones.addActionListener(new_game_listener);
        
        JLabel num_of_stones_title = new JLabel("Vyberte velikost hracího pole:");
        
        panel_grid.add(num_of_stones_title);
        panel_grid.add(this.box_stones);
        
        // přidáme oddělovač
        
        panel_grid.add(new JLabel("__________________________________________"));
        
        // přidáme volbu na počet pokladů
        
        this.twelve_treasures = new JRadioButton("12 Pokladů");
        this.twelve_treasures.setSelected(true);

        this.twenty_four_treasures = new JRadioButton("24 Pokladů");

        ButtonGroup num_of_treasures = new ButtonGroup();
        num_of_treasures.add(twelve_treasures);
        num_of_treasures.add(twenty_four_treasures);

        this.twelve_treasures.addActionListener(new_game_listener);
        this.twenty_four_treasures.addActionListener(new_game_listener);
        
        JLabel num_of_treasures_title = new JLabel("Vyberte počet pokladů:");
        
        panel_grid.add(num_of_treasures_title);
        panel_grid.add(this.twelve_treasures);
        panel_grid.add(this.twenty_four_treasures);
        
        // přidáme oddělovač
        
        panel_grid.add(new JLabel("__________________________________________"));
        
        // přidáme volbu na velikost okna
        
        final JLabel rozliseni = new JLabel("Velikost okna hry: " + (Integer.toString(Game.getReferenceWindowWidth())) + " x " +(Integer.toString(Game.getReferenceWindowHeight()))  );
        panel_grid.add(rozliseni);

        settings_item.add(panel_grid);
        settings_item.add(Box.createRigidArea(new Dimension(10,10)));
        
        final int FPS_MIN = 50;
        final int FPS_MAX = 150;
        final int FPS_INIT = 100;
        
        // obsluha slideru pro nastavení velikosti okna
        JSlider framesPerSecond = new JSlider(JSlider.HORIZONTAL, FPS_MIN, FPS_MAX, FPS_INIT);
        framesPerSecond.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider)e.getSource();
                double val = (double) source.getValue() / 100;
                rozliseni.setText("Velikost okna hry: " + (Integer.toString((int) ((double) Game.getReferenceWindowWidth() * val))) + " x " +(Integer.toString( (int) ((double) Game.getReferenceWindowHeight() * val)) ));
                    
                NewGameJFRame.reference_window_width = (int) ((double) Game.getReferenceWindowWidth() * val);
                NewGameJFRame.reference_window_height = (int) ((double) Game.getReferenceWindowHeight() * val);
            }
        });

        framesPerSecond.setMajorTickSpacing(50);
        framesPerSecond.setMinorTickSpacing(50);
        framesPerSecond.setPaintTicks(true);
        
        Hashtable<Integer, JLabel> labelTable = new Hashtable<Integer, JLabel>();
        labelTable.put( new Integer( FPS_MIN ), new JLabel("malá"));
        labelTable.put( new Integer( FPS_INIT ), new JLabel("střední") );
        labelTable.put( new Integer( FPS_MAX ), new JLabel("velká"));
        framesPerSecond.setLabelTable( labelTable );

        framesPerSecond.setPaintLabels(true);
        framesPerSecond.setAlignmentX(Component.CENTER_ALIGNMENT);
        settings_item.add(framesPerSecond);
        
        // přidáme mezeru
        
        settings_item.add(new JLabel("     "));
        settings_item.add(new JLabel("     "));
        
        // přidáme tlačítko pro vytvoření hry
                            
        this.confirm_button = new JButton();
        this.confirm_button.setText("Vytvořit novou hru");
        
        // obsluha tlačítka pro vytvoření nové hry
        this.confirm_button.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            JButton btn = (JButton) e.getSource();
            
            // nastavené parametry hry zapíšeme do Třídy Game
            Game.setNumberOfPlayers(NewGameJFRame.number_of_players); 
            Game.setSizeOfBoard(NewGameJFRame.size_of_board);
            Game.setNumberOfTreasures(NewGameJFRame.number_of_treasures);
            Game.setWindowWidth(NewGameJFRame.reference_window_width);
            Game.setWindowHeight(NewGameJFRame.reference_window_height);
            
            NewGameJFRame.reference_window_width = 1088;
            NewGameJFRame.reference_window_height = 680;
            Game.initGame();
            
            // nakonec po vytvoření hry povolíme uložení hry
            saveGameAction.setEnabled(true);
            dispose();
        }
        });
        confirm_button.setAlignmentX(Component.CENTER_ALIGNMENT);
        settings_item.add(confirm_button);
        
        panel.add(settings_item);
        this.add(panel);
        
        this.pack();
        
    }
    
    /**
     * Vnořená Třída NewGameListener pro obluhu operací nad komponentami nadřazené třídy
     * @author Jan Herec, Pavel Juhaňák
     */
    private class NewGameListener implements ActionListener {

        /**
         * Metoda pro obsluhu událostí (změna paramretrů hry)
         */
        public void actionPerformed(ActionEvent e) {
            
            // obsluha změny počtu hráčů
            if (e.getSource() == box_players) {
                NewGameJFRame.number_of_players = Integer.parseInt(((String) box_players.getSelectedItem()).substring(0, 1));
            } 
            // obsluha změny velikosti hrací desky
            else if (e.getSource() == box_stones) {
                int size_of_b = Integer.parseInt(((String) box_stones.getSelectedItem()).substring(0, 1));
                if (size_of_b == 1) {
                    NewGameJFRame.size_of_board = 11;
                } else {
                    NewGameJFRame.size_of_board = size_of_b;
                }
            }
            // obsluha změny počtu pokladů
            else if (e.getSource() == twelve_treasures) {
                NewGameJFRame.number_of_treasures = 12;
            }
            else if (e.getSource() == twenty_four_treasures) {
                NewGameJFRame.number_of_treasures = 24;
            }
        }
    }

}