/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu MazeBoard, viz dokumentační komentář této třídy 
 */

package ija.game.board;

import ija.game.*;
import ija.game.treasure.Treasure;
import java.util.*;

/**
 * Třída reprezentuj hrací desku. Deska je rozdělena na políčka (viz. MazeField)
 * a je čtvercová o rozměru n (n řádku + n sloupců). 
 * Umožňuje generovat novou hru a posunovat kameny na řádcích nebo sloupcích
 * @see ija.game.board.MazeField
 * @author Jan Herec, Pavel Juhaňák
 */
public class MazeBoard {
    
    private MazeField maze_board[][]; // hrací deska složená z políček
    private int size;     // velikost hrací desky
    private MazeStone free_stone;   // volný hrací kámen
    
    /**
     * Konstruktor vytvoří hrací plochu o velikosti n. Při inicializaci se vytvoří políčka (instance třídy MazeField)
     * a umístí se na správné pozice. Kameny se nevytvářejí. Vzniká pouze prázdá hrací plocha.
     * @param n velikost hracího pole které se bude vytvářet
     */
    private MazeBoard(int n) {
        this.maze_board = new MazeField[n][n];
        this.size = n;
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                maze_board[ row ][ col ] = new MazeField(row, col);
            }
        }
        
        this.free_stone = null;
    }
    
    /**
     * Vytvoří hrací plochu o velikosti n tím, že volá konstruktor
     * @param n velikost hracího pole které se bude vytvářet
     * @return vrací nově vytvořené hrací pole
     */
    public static MazeBoard createMazeBoard(int n) {
        return new MazeBoard(n);
    }
    
    /**
     * Vytvoří novou hru. Generuje hrací kameny a umísťuje je na políčka. 
     * Současně vygeneruje jeden volný kámen, který uchovává mimo políčka.
     */
    public void newGame() {
        Random rand_generator = new Random(); // generátor pseudonáhodných čísel
        int randIndex;                        // náhodný index do pole kamenů
        String[] stonesTypes = {"C","L","F"}; // typy kamenů

        int row, col, i;

        Stack<String> stack = new Stack<String>(); // zasobnik vkladanych kamenu

        int c = 0, l = 0, f = 0; // pocet jednotlivych typu kamenu

        /**
        * Spocitani kolik ma kterych kamenu byt
        */
        if (this.size == 5) {
            c = 9 - 4;
            l = 9;
            f = 8 - 5;
        } else if (this.size == 7) {
            c = 17 - 4;
            l = 17;
            f = 16 - 12;
        } else if (this.size == 9) {
            c = 27 - 4;
            l = 28;
            f = 27 - 21;
        } else {
            c = 41 - 4;
            l = 41;
            f = 40 - 32;
        }

        /**
        * Ulozeni typu kamenu do zasobniku
        */
        for (i = 0; i < c; i++) {
            stack.push("C");
        }
        for (i = 0; i < l; i++) {
            stack.push("L");
        }
        for (i = 0; i < f; i++) {
            stack.push("F");
        }
        
        /**
        * Zamicha kameny
        */
        Collections.shuffle(stack);
        Collections.shuffle(stack);
        Collections.shuffle(stack);

        /**
        * Prochazi policka a vklada na ne prislusne kameny
        * na kamenech defaultne neni zadny poklad
        */
        for (row = 0; row < this.size; row++) {
            for (col = 0; col < this.size; col++) {
                if (row == 0 && col == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("C"));
                    this.maze_board[ row ][ col ].getStone().turnRight();
                    this.maze_board[ row ][ col ].getStone().turnRight();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));

                } else if (row == 0 && col == this.size - 1) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("C"));
                    this.maze_board[ row ][ col ].getStone().turnLeft();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if (row == this.size - 1 && col == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("C"));
                    this.maze_board[ row ][ col ].getStone().turnRight();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if (row == this.size - 1 && col == this.size - 1) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("C"));

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if (row == 0 && (col % 2) == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("F"));
                    this.maze_board[ row ][ col ].getStone().turnRight();
                    this.maze_board[ row ][ col ].getStone().turnRight();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if (row == this.size - 1 && (col % 2) == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("F"));

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if ((row % 2) == 0 && col == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("F"));
                    this.maze_board[ row ][ col ].getStone().turnRight();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if ((row % 2) == 0 && col == this.size - 1) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("F"));
                    this.maze_board[ row ][ col ].getStone().turnLeft();

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));
                
                } else if ((row % 2) == 0 && (col % 2) == 0) {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create("F"));

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));

                    randIndex = rand_generator.nextInt(4);
                    for (i = 0; i < randIndex; i++) {
                        this.maze_board[ row ][ col ].getStone().turnRight();
                    }
                
                } else {
                    this.maze_board[ row ][ col ].putStone( MazeStone.create( stack.pop()));

                    this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(0));

                    randIndex = rand_generator.nextInt(4);
                    for (i = 0; i < randIndex; i++) {
                        this.maze_board[ row ][ col ].getStone().turnRight();
                    }
                }
                //System.out.println(this.maze_board[row][col].getStone().type);
            }
        }
        
        /**
        * vygenerujeme volný kámen
        */
        this.free_stone = MazeStone.create(stack.pop());
        this.free_stone.putTreasure(Treasure.getTreasure(0));

        /**
        * Vlozi poklady na kameny
        */
        int num_of_stones = this.size * this.size;

        for (i = Game.getNumberOfTreasures(); i > 0; i--) {
            randIndex = rand_generator.nextInt(num_of_stones);

            row = randIndex / Game.getSizeOfBoard();
            col = randIndex % Game.getSizeOfBoard();

            if (this.maze_board[ row ][ col ].getStone().getTreasure() == Treasure.getTreasure(0)) {
                this.maze_board[ row ][ col ].getStone().putTreasure(Treasure.getTreasure(i));
            } else {
                i++;
            }
        }
    }
    
    /**
     * Vrací políčko na pozici [r, c]. Pozor, pole se indexuje od 0
     * @param r pozice políčka na řádku
     * @param c pozice políčka na sloupci
     * @return vrací políčko na dané pozici. V případě chyby (mimo rozsah) vrací null.
     */
    public MazeField get(int r, int c){
        // pokud požadované políčko leží mimo rozsah
        if (r < 0 || c < 0 || r >= this.size || c >= this.size) {
            return null;
        }
        return this.maze_board[ r ][ c ];
    }

    /**
     * Vloží stone na políčko na pozici [r, c]
     * @param r pozice políčka na řádku
     * @param c pozice políčka na sloupci
     * @param stone vkládaný kámen
     */
    public void putStone(int r, int c, MazeStone stone) {
        this.maze_board[ r ][ c ].putStone(stone);
    }
    
    /**
     * Vrací volný kámen
     * @return vrací volný kámen. Pokud takový kámen není (hra nebyla vytvořena) vrací null.
     */
    public MazeStone getFreeStone(){
        if(this.free_stone == null) {
            return null;
        }
        return this.free_stone;
    }
    
    /**
    * Metoda vloží na hrací desku volný kámen
    * @param free_stone volný kámen
    */
    public void putFreeStone(MazeStone free_stone){
        this.free_stone = free_stone;
    }
    
    /**
     * Provede vložení volného kamene na pozici políčka mf. 
     * Podle pozice políčka mf se provede příslušné posunutí kamenu:
     *   • první řádek [1, c] – kameny se posunou dolů ve sloupci c, pokud je c sudé
     *   • poslední řádek [n, c] – kameny se posunou nahoru ve sloupci c, pokud je c sudé
     *   • první sloupec [r, 1] – kameny se posunou doprava na řádku r, pokud je r sudé
     *   • poslední sloupec [r, n] – kameny se posunou doleva na řádku r, pokud je r sudé
     * Na políčko mf se vloží volný kámen a další kameny se posunou. 
     * Kámen, který je tímto postupem vysunut, se stává volným kamenem.
     * Pokud políčko mf nesplňuje výše uvedené podmínky, nedělá metoda nic.
     * @param mf políčko desky
     */
    public void shift(MazeField mf) {
        MazeStone tmp = this.free_stone;
        int row = mf.row();
        int col = mf.col();

        // pokud je políčko mf na prvním řádku a pokud je na sudém sloupci
        if ((row+1) == 1 && ((col+1) % 2) == 0) {
            this.free_stone = this.maze_board[ this.size - 1 ][ col ].getStone();
            // posuneme kameny ve sloupci dolů, takže procházíme řádky v daném sloupci směrem dolů a provádíme shift
            for (int r = this.size - 1; r > 0; r--) {
                this.maze_board[ r ][ col ].putStone( this.maze_board[ r - 1 ][ col ].getStone());
            }
        }
        // pokud je políčko mf na posledním řádku a pokud je na sudém sloupci
        else if ((row+1) == this.size && ((col+1) % 2) == 0) {
            this.free_stone = this.maze_board[ 0 ][ col ].getStone();
            // posuneme kameny ve sloupci nahoru, takže procházíme řádky v daném sloupci směrem nahoru a provádíme shift
            for (int r = 0; r < this.size - 1; r++) {
                this.maze_board[ r ][ col ].putStone( this.maze_board[ r + 1 ][ col ].getStone());
            }
        }
        // pokud je políčko mf na sudém řádku a pokud je na prvním sloupci
        else if (((row+1) % 2) == 0 && (col+1) == 1) {
            this.free_stone = this.maze_board[ row ][ this.size - 1 ].getStone();
            // posuneme kameny v řádku doprava, takže procházíme sloupce v daném řádku směrem doprava a provádíme shift
            for (int c = this.size - 1; c > 0; c--) {
                this.maze_board[ row ][ c ].putStone( this.maze_board[ row ][ c - 1 ].getStone());
            }
        }
        // pokud je políčko mf na sudém řádku a pokud je na posledním sloupci
        else if (((row+1) % 2) == 0 && (col+1) == this.size) {
            this.free_stone = this.maze_board[ row ][ 0 ].getStone();
            // posuneme kameny v řádku doleva, takže procházíme sloupce v daném řádku směrem doleva a provádíme shift
            for (int c = 0; c < this.size - 1; c++) {
                this.maze_board[ row ][ c ].putStone( this.maze_board[ row ][ c + 1 ].getStone());
            }
        } else {
            return;
        }

        this.maze_board[ row ][ col ].putStone(tmp);
    }
}
