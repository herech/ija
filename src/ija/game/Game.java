/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu Game, viz dokumentační komentář této třídy 
 */
 
package ija.game;

import ija.game.board.*;
import ija.game.board.MazeStone.*;
import ija.game.treasure.*;
import ija.gui.*;
import ija.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.awt.*;

/**
 * Třída Game představuje celou hru, obsahuje stavové proměnné hry, hráče, poklady, hrací desku atd.
 * @author Jan Herec, Pavel Juhaňák
 */

public class Game {
    
    private static int reference_window_width = 1088;
    private static int reference_window_height = 680;
    
    private static int width_stone;
    private static int height_stone;

    /** Informace o parametrech hry */
    private static int number_of_players;
    private static int size_of_board;
    private static int number_of_treasures;
    
    private static int window_width = 1088;
    private static int window_height = 680;

    /** mapujeme kameny na jejich typy */
    private static Map<String, Integer> stone_map = new HashMap<String, Integer>();

    /**
     * Ulozene vsechy pouzivane obrazky
     */
    private static Image arrow;
    private static Image arrow_hover;
    private static Image[] stones = new Image[3];
    private static Image[] treasures = new Image[25];
    private static Image[] cards = new Image[26];
    private static Image[] figures = new Image[5];
    private static Image card_backside;
    private static Image card_non_turned;
    
    /** Komponenty hry */
    private static ArrayList<JButton> button_stones = new ArrayList<JButton>(); 
    private static ArrayList<JLabel> label_turned_cards = new ArrayList<JLabel>(); 
    private static JLabel free_stone_label;
    private static JLabel label_actual_card;
    private static MazeBoard board;
    private static CardPack card_pack;
    private static Player[] players = new Player[4];

    /**
     * Promenne pro rozpoznani stavu hry
     */
    private static boolean can_interact = true;
    private static int last_shift_row = -1;
    private static int last_shift_col = -1;
    private static boolean player_did_shift = false;
    
    private static int current_player;
    private static boolean winner;
    private static boolean can_shift;
    
    /** Uložené okno hry a hlavní panel hry */
    private static GuiJPanel j_panel_instance;
    private static GUI j_frame_instance;

    /**
     * Metoda inicialiazuje hru
     */
    public static void initGame() {
        
        // resetujeme poslední shiftovaný řádek a sloupec
        Game.last_shift_row = -1;
        Game.last_shift_col = -1;
        Game.player_did_shift = false;
        
        // vytvoříme hráče
        Game.createPlayers();
        
        // vytvoříme set pokladů
        Treasure.createSet();
        
        // vytvoříme balíček karet a zamícháme jej
        Game.card_pack = new CardPack(Game.number_of_treasures);
        Game.card_pack.shuffle();
        
        // vytvoříme desku
        Game.board = MazeBoard.createMazeBoard(Game.size_of_board);
        Game.board.newGame();
        
        // dáme prvnímu hráči kartu
        Game.popCardForCurrentPlayer();

        // vyčistíme všechny pole kam ukládáme komponenty hry
        button_stones.clear();
        label_turned_cards.clear();
        Command.clear();
        
        // vytvoříme a vykreslíme komponenty hry 
        Game.j_panel_instance.removeAll();
        Game.j_panel_instance.createBoard();
        Game.j_panel_instance.redrawBoard();
        Game.j_panel_instance.redrawTurnedCards();

    }
    
    /** 
     * Metoda načte na začátku programu zdroje
     */
    public static void loadImages() {
        int i;

        
        // Nacteni sipky
        Game.arrow = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/sipka4.png")).getImage();
        Game.arrow_hover = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/sipka3.png")).getImage();

        // Mapovani aby se dalo indexovat pismenem
        Game.stone_map.put("C" , Integer.valueOf(0));
        Game.stone_map.put("L" , Integer.valueOf(1));
        Game.stone_map.put("F" , Integer.valueOf(2));

        // Nacteni kamenu
        Game.stones[stone_map.get("C")] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/stones/C-path1.png")).getImage();
        Game.stones[stone_map.get("L")] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/stones/L-path1.png")).getImage();
        Game.stones[stone_map.get("F")] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/stones/F-path1.png")).getImage();

        // Nacteni pokladu
        for (i = 0; i <= 24; i++) {
            Game.treasures[i] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/treasures/" + i + ".png")).getImage();
        }

        // Nacteni karet
        for (i = 1; i <= 24; i++) {
            Game.cards[i] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/cards/" + i + ".png")).getImage();
        }
        Game.cards[25] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/cards/non_turned1.png")).getImage();
        
        // Nacteni figurek
        for (i = 1; i <= 4; i++) {
            Game.figures[i] = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/figures/foo" + i + ".png")).getImage();
        }
        
        // Nacteni pozadí karty
        Game.card_backside = new ImageIcon(new Object() { }.getClass().getEnclosingClass().getResource("/graphics/cards/backside2.png")).getImage();
    
        // Nacteni neotočené karty
        Game.card_non_turned = Game.cards[25];
    }

    /**
     * Vrati typ kamene na pozici
     * @param row souřadnice x pozice na desce
     * @param col souřadnice y pozice na desce
     * @return typ kamene na pozici
     */
    public static String getType(int row, int col) {
        return Game.board.get(row, col).getStone().getType();
    }
    
    /**
     * Vrati typ volného kamene
     * @return typ volného kamene
     */
    public static String getTypeOfFreeStone() {
        return Game.board.getFreeStone().getType();
    }

    /**
     * Vrati poklad na pozici
     * @param row souřadnice x pozice na desce
     * @param col souřadnice y pozice na desce
     * @return poklad
     */
    public static Treasure getTreasure(int row, int col) {
        return Game.board.get(row, col).getStone().getTreasure();
    }

    /**
     * Nastavi nulty poklad na pozici
     * @param row souřadnice x pozice na desce
     * @param col souřadnice y pozice na desce
     */
    public static void putZeroTreasure(int row, int col) {
        Game.board.get(row, col).getStone().putTreasure(Treasure.getTreasure(0));
    }

    /**
     * Vrati kod pokladu na pozici
     * @param row souřadnice x pozice na desce
     * @param col souřadnice y pozice na desce
     * @return kod pokladu
     */
    public static int getTreasCode(int row, int col) {
        return Game.getTreasure(row, col).getTreasureCode();
    }

    /**
     * Vrati kod volneho pokladu
     * @return kod pokladu
     */
    public static int getFreeTreasCode() {
        return Game.board.getFreeStone().getTreasure().getTreasureCode();
    }

    /**
     * Vrati o kolik ma byt kamen orotovan
     * @param row souřadnice x pozice na desce
     * @param col souřadnice y pozice na desce
     * @return rotace kamene
     */
    public static int getRotation(int row, int col) {
        return Game.board.get(row, col).getStone().getRotation();
    }
    
    /**
     * Vrati o kolik ma byt volný kamen orotovan
     * @return rotace kamene
     */
    public static int getRotationOfFreeStone() {
        return Game.board.getFreeStone().getRotation();
    }
    
    // ZAČÁTEK SEKCE GETTERŮ A SETTERŮ, KTERÉ POUZE VRACÍ NEBO UKLÁDÁJÍ DO TŘÍDNÍCH PROMĚNNÝCH A ATRIBUTŮ
  
    public static void redrawBoard() {
        Game.j_panel_instance.redrawBoard();
    }
  
    public static int getWindowWidth() {
        return Game.window_width;
    }

    public static void setWindowWidth(int width) {
        Game.window_width = width;
    }

    public static int getReferenceWindowWidth() {
        return Game.reference_window_width;
    }

    public static void setReferenceWindowWidth(int width) {
        Game.reference_window_width = width;
    }

    public static int getReferenceWindowHeight() {
        return Game.reference_window_height;
    }

    public static void setReferenceWindowHeight(int height) {
        Game.reference_window_height = height;
    }

    public static int getWindowHeight() {
        return Game.window_height;
    }

    public static void setWindowHeight(int height) {
        Game.window_height = height;
    }

    public static int getWidthStone() {
        return Game.width_stone;
    }

    public static void setWidthStone(int width_stone) {
        Game.width_stone = width_stone;
    }

    public static int getHeightStone() {
        return Game.height_stone;
    }

    public static void setHeightStone(int height_stone) {
        Game.height_stone = height_stone;
    }

    public static Image getArrow() {
        return Game.arrow;
    }

    public static void setArrow(Image arrow) {
        Game.arrow = arrow;
    }

    public static Image getArrowHover() {
        return Game.arrow_hover;
    }

    public static void setArrowHover(Image arrow_hover) {
        Game.arrow_hover = arrow_hover;
    }

    public static Image getStones(int i) {
        return Game.stones[i];
    }

    public static Image getTreasures(int i) {
        return Game.treasures[i];
    }

    public static Image getCards(int i) {
        return Game.cards[i];
    }

    public static Image getFigures(int i) {
        return Game.figures[i];
    }

    public static Image getCardBackside() {
        return Game.card_backside;
    }

    public static Map<String, Integer> getStoneMap() {
        return Game.stone_map;
    }

    public static ArrayList<JButton> getButtonStones() {
        return Game.button_stones;
    }

    public static ArrayList<JLabel> getLabelTurnedCards() {
        return Game.label_turned_cards;
    }

    public static void setCardBackside(Image card_backside) {
        Game.card_backside = card_backside;
    }

    public static Image getCardNonTurned() {
        return Game.card_non_turned;
    }

    public static void setCardNonTurned(Image card_non_turned) {
        Game.card_non_turned = card_non_turned;
    }

    public static int getNumberOfPlayers() {
        return Game.number_of_players;
    }
    
    public static void setNumberOfPlayers(int num_of_pl) {
        Game.number_of_players = num_of_pl;
    }

    public static int getSizeOfBoard() {
        return Game.size_of_board;
    }
    
    public static void setSizeOfBoard(int size_of_b) {
        Game.size_of_board = size_of_b;
    }
    
    public static int getNumberOfTreasures() {
        return Game.number_of_treasures;
    }

    public static void setNumberOfTreasures(int num_of_tr) {
        Game.number_of_treasures = num_of_tr;
    }
    
    public static boolean getCanInteract() {
        return Game.can_interact;
    }

    public static void setCanInteract(boolean can_interact) {
        Game.can_interact = can_interact;
    }

    public static JLabel getFreeStoneLabel() {
        return Game.free_stone_label;
    }

    public static void setFreeStoneLabel(JLabel free_stone_label) {
        Game.free_stone_label = free_stone_label;
    }

    public static JLabel getLabelActualCard() {
        return Game.label_actual_card;
    }

    public static void setLabelActualCard(JLabel label_actual_card) {
        Game.label_actual_card = label_actual_card;
    }

    public static boolean getPlayerDidShift() {
        return Game.player_did_shift;
    }
    
    public static void setPlayerDidShift(boolean player_did_shift) {
        Game.player_did_shift = player_did_shift;
    }
    
    public static boolean getCanShift() {
        return Game.can_shift;
    }
    
    public static void setCanShift(boolean can_shift) {
        Game.can_shift = can_shift;
    }
    
    public static int getLastShiftRow() {
        return Game.last_shift_row;
    }
    
    public static void setLastShiftRow(int last_shift_row) {
        Game.last_shift_row = last_shift_row;
    }
    
    public static int getLastShiftCol() {
        return Game.last_shift_col;
    }
    
    public static void setLastShiftCol(int last_shift_col) {
        Game.last_shift_col = last_shift_col;
    }
    
    public static int getIndexCurrenPlayer() {
        return Game.current_player;
    }
    
    public static void setIndexCurrenPlayer(int current_player) {
        Game.current_player = current_player;
    }

    public static boolean isWinner() {
        return Game.winner;
    }
    
    public static void setIsWinner(boolean winner) {
        Game.winner = winner;
    }

    public static CardPack getCardPack() {
        return Game.card_pack;
    }
    
    public static void setCardPack(CardPack card_pack) {
        Game.card_pack = card_pack;
    }

    public static MazeBoard getBoard() {
        return Game.board;
    }
    
    public static void setBoard(MazeBoard board) {
        Game.board = board;
    }
    
    public static Player getPlayer(int index) {
        return Game.players[index];
    }

    public static void setPlayer(int index, Player pl) {
        Game.players[index] = pl;
    }

    public static GuiJPanel getJPanelInstance() {
        return Game.j_panel_instance;
    }

    public static void setJPanelInstance(GuiJPanel j_panel_instance) {
        Game.j_panel_instance = j_panel_instance;
    }

    public static GUI getJFramelInstance() {
        return Game.j_frame_instance;
    }

    public static void setJFrameInstance(GUI j_frame_instance) {
        Game.j_frame_instance = j_frame_instance;
    }

    public static void clearButtonStones() {
        Game.button_stones.clear();
    }

    public static void clearLabelTurnedCards() {
        Game.label_turned_cards.clear();
    }
    
    // KONEC SEKCE GETTERŮ A SETTERŮ, KTERÉ POUZE VRACÍ NEBO UKLÁDÁJÍ DO TŘÍDNÍCH PROMĚNNÝCH A ATRIBUTŮ

    /** 
     * Metoda ověří jestli můžeme provést posun na řádku row a sloupci col 
     * @param row řádek
     * @param col sloupec
     * @return true nebo false
     */
    public static boolean canShift(int row, int col) {
        if (Game.can_shift) {
            // pokud chce hráč provést reverzní posunutí vzhledem k poslednímu posunutí, odmítneme to
            if (Game.last_shift_row == 0 && row == Game.size_of_board - 1 && Game.last_shift_col == col) {
                return false;
            }
            else if (Game.last_shift_col == 0 && col == Game.size_of_board - 1 && Game.last_shift_row == row) {
                return false;
            }
            else if (Game.last_shift_row == Game.size_of_board - 1 && row == 0 && Game.last_shift_col == col) {
                return false;
            }
            else if (Game.last_shift_col == Game.size_of_board - 1 && col == 0 && Game.last_shift_row == row) {
                return false;
            }
            // pokud chce hráč nechce provést reverzní posunutí vzhledem k poslednímu posunutí, povolíme to
            else {
                return true;
            }
            
        }
        else return false;
    }
    
    /** Metoda vytvoří nové hráče */
    private static void createPlayers() {
        Game.current_player = 0;
        Game.winner = false;
        Game.can_shift = true;
        Game.can_interact = true;  
        for (int i = 0; i < Game.number_of_players; i++) {
            Game.players[i] = new Player();
            if (i == 0) {
                Game.players[i].setRow(0);
                Game.players[i].setCol(0);
            } else if (i == 1) {
                Game.players[i].setRow(0);
                Game.players[i].setCol(Game.size_of_board - 1);
            } else if (i == 2) {
                Game.players[i].setRow(Game.size_of_board - 1);
                Game.players[i].setCol(Game.size_of_board - 1);
            } else {
                Game.players[i].setRow(Game.size_of_board - 1);
                Game.players[i].setCol(0);
            }
        }
    }
    
    /**
     * Metoda vrátí současného hráče 
     * @return současný hráč
     * */
    public static Player getCurrentPlayer() {
        return Game.players[Game.current_player];
    }
    
    /** Metoda vytáhne z vrcholu balíčku karet kartu pro současného hráče */
    public static void popCardForCurrentPlayer() {
        Player pl = Game.players[Game.current_player];
        pl.setTreasureCard(Game.card_pack.popCard());
    }
    
    /** 
     * Metoda vrátí současnou kartu pokladu který hráč hledá 
     * @return Obrázek karty která zobrazuje poklad jež hráč hledá
     **/
    public static Image getTreasureCardImageForCurrentPlayer() {
        return Game.cards[Game.getCurrentPlayer().getTreasureCard().getTreasure().getTreasureCode()];
    }

    /**
    * Posune hrace ve stejnem smeru jak kameny
    * @param row řádek kde se provádí posun
    * @param col sloupec kde se provádí posun
    */
    public static void playerShift(int row, int col) {
        int new_pos;
        if ((row+1) == 1 && ((col+1) % 2) == 0) {
            // hrace, ktery je v tomto sloupci posuneme ve sloupci dolů
            for (int i = 0; i < Game.number_of_players; i++) {
                if (Game.players[i].getCol() == col) {
                    new_pos = (Game.players[i].getRow() + 1) % Game.size_of_board;
                    Game.players[i].setRow(new_pos);
                }
            }
        } else if ((row+1) == Game.size_of_board && ((col+1) % 2) == 0) {
            // hrace, ktery je v tomto sloupci posuneme ve sloupci nahoru
            for (int i = 0; i < Game.number_of_players; i++) {
                if (Game.players[i].getCol() == col) {
                    new_pos = (Game.players[i].getRow() - 1) % Game.size_of_board;
                    new_pos = new_pos < 0 ? Game.size_of_board + new_pos : new_pos;
                    Game.players[i].setRow(new_pos);
                }
            }
        } else if (((row+1) % 2) == 0 && (col+1) == 1) {
            // hrace, ktery je v tomto radku posuneme v řádku doprava
            for (int i = 0; i < Game.number_of_players; i++) {
                if (Game.players[i].getRow() == row) {
                    new_pos = (Game.players[i].getCol() + 1) % Game.size_of_board;
                    Game.players[i].setCol(new_pos);
                }
            }
        } else if (((row+1) % 2) == 0 && (col+1) == Game.size_of_board) {
            // hrace, ktery je v tomto radku posuneme v řádku doleva
            for (int i = 0; i < Game.number_of_players; i++) {
                if (Game.players[i].getRow() == row) {
                    new_pos = (Game.players[i].getCol() - 1) % Game.size_of_board;
                    new_pos = new_pos < 0 ? Game.size_of_board + new_pos : new_pos;
                    Game.players[i].setCol(new_pos);
                }
            }
        } else {
            return;
        }
        Game.can_shift = false;
        Game.player_did_shift = true;
        Game.last_shift_row = row;
        Game.last_shift_col = col;
    }

    /**
     * Metoda pomocí algoritmu BFS ověří a případně posune hráče na dané políčko
     * @param row_f souřadnice x políčka kam chce hráč přejít
     * @param col_f souřadnice y políčka kam chce hráč přejít
     * @return true nebo false v závislosti na tom jestli je možné na políčko přejít
     */
    public static boolean canGoOn(int row_f, int col_f) {
        /**
         * Lokalni trida pro souradnice
         */
        class MazePoint {
            public int row;
            public int col;

            public MazePoint(int row, int col) {
                this.row = row;
                this.col = col;
            }

            public MazePoint(MazePoint p) {
                this.row = p.row;
                this.col = p.col;
            }

            public MazePoint() {}

            @Override
            public boolean equals(Object o) {
                if (o instanceof MazePoint) {
                    MazePoint point = (MazePoint) o;
                    if (this.row == point.row && this.col == point.col) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            @Override
            public int hashCode() {
                return this.row * 1000 + this.row;
            }
        }

        int row = Game.getCurrentPlayer().getRow();
        int col = Game.getCurrentPlayer().getCol();

        boolean can_go_on = false;

        MazePoint final_point = new MazePoint(row_f, col_f);
        MazePoint point = new MazePoint(row, col);
        MazePoint tmp_point = new MazePoint();

        LinkedList<MazePoint> open = new LinkedList<MazePoint>();
        LinkedList<MazePoint> closed = new LinkedList<MazePoint>();

        open.clear();
        closed.clear();

        open.add(point);

        if (final_point.equals(point)) {
            can_go_on = true;
            open.removeFirst();
        }

        while (open.size() > 0) {
            point = open.removeFirst();

            row = point.row;
            col = point.col;
 
            if (row - 1 >= 0 && Game.board.get(row, col).getStone().canGo(CANGO.UP) && Game.board.get(row - 1, col).getStone().canGo(CANGO.DOWN)) {
                tmp_point.row = row - 1;
                tmp_point.col = col;

                if (final_point.equals(tmp_point)) {
                    can_go_on = true;
                    break;
                }

                if (!open.contains(tmp_point) && !closed.contains(tmp_point)) {
                    open.addLast(new MazePoint(tmp_point));
                }
            }

            if (row + 1 < Game.size_of_board && Game.board.get(row, col).getStone().canGo(CANGO.DOWN) && Game.board.get(row + 1, col).getStone().canGo(CANGO.UP)) {
                tmp_point.row = row + 1;
                tmp_point.col = col;

                if (final_point.equals(tmp_point)) {
                    can_go_on = true;
                    break;
                }

                if (!open.contains(tmp_point) && !closed.contains(tmp_point)) {
                    open.addLast(new MazePoint(tmp_point));
                }
            }

            if (col - 1 >= 0 && Game.board.get(row, col).getStone().canGo(CANGO.LEFT) && Game.board.get(row, col - 1).getStone().canGo(CANGO.RIGHT)) {
                tmp_point.row = row;
                tmp_point.col = col - 1;

                if (final_point.equals(tmp_point)) {
                    can_go_on = true;
                    break;
                }

                if (!open.contains(tmp_point) && !closed.contains(tmp_point)) {
                    open.addLast(new MazePoint(tmp_point));
                }
            }

            if (col + 1 < Game.size_of_board && Game.board.get(row, col).getStone().canGo(CANGO.RIGHT) && Game.board.get(row, col + 1).getStone().canGo(CANGO.LEFT)) {
                tmp_point.row = row;
                tmp_point.col = col + 1;

                if (final_point.equals(tmp_point)) {
                    can_go_on = true;
                    break;
                }

                if (!open.contains(tmp_point) && !closed.contains(tmp_point)) {
                    open.addLast(new MazePoint(tmp_point));
                }
            }

            closed.add(new MazePoint(point));
        }

        if (can_go_on) {
            Game.getCurrentPlayer().setRow(row_f);
            Game.getCurrentPlayer().setCol(col_f);
            
            return true;
        }
        
        return false;
    }

    /**
     * Zkontroluje jestli vzal poklad/vytahne dalsi/oznami vyhru
     * @return true nebo false v závislosti na tom jestli hráč vzal poklad
     */
    public static boolean didIJustTakeTreasure() {
        Treasure player_treasure = Game.getCurrentPlayer().getTreasureCard().getTreasure();
        Treasure stone_treasure = Game.getTreasure(Game.getCurrentPlayer().getRow(), Game.getCurrentPlayer().getCol());

        // je hráč na políčku kde je poklad jež hledá?
        if (player_treasure == stone_treasure) {
            Game.getCurrentPlayer().addTurnedCard(player_treasure.getTreasureCode());

            Game.putZeroTreasure(Game.getCurrentPlayer().getRow(), Game.getCurrentPlayer().getCol());

            // nevyhrál hráč?
            if (Game.getCurrentPlayer().getNumOfTreasures() <= Game.number_of_treasures / Game.number_of_players) {
                Game.j_panel_instance.showCurrentCard();
                Game.can_interact = false;
                
                // timer pomocí kterého na chvíli zobrazíme kartu otočenou
                Timer timer = new Timer(2500, new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {                    
                        Game.j_panel_instance.hideCurrentCard();      
                        GuiJPanel.setColorOfLabelPlayer();      
                        Game.j_panel_instance.redrawTurnedCards();  
                        Game.can_interact = true;  
                    }
                });
                timer.setRepeats(false);
                timer.start(); 
                
                Game.popCardForCurrentPlayer();
            } 
            // vyhrál hráč?
            else {
                Game.can_shift = false;
                Game.winner = true;
                Game.j_panel_instance.redrawTurnedCards();
            }
            
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Ukonci tah a prepne na dalsiho hrace
     */
    public static void endMove() {
        Game.current_player = (Game.current_player + 1) % Game.number_of_players;

        if (Game.getCurrentPlayer().getTreasureCard() == null) {
            Game.popCardForCurrentPlayer();
        }

        Game.can_shift = true;
        
        if (Game.player_did_shift == false) {
            Game.last_shift_row = -1;
            Game.last_shift_col = -1;
        }
        
        Game.player_did_shift = false;

    }
    
    /**
     * Vrátí číslo hráče - pořadí
     * @return pořadí hráče
     */
    public static int getOrder() {
        return Game.current_player + 1;
    }
}
