/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu MazeField, viz dokumentační komentář této třídy 
 */

package ija.game.board;

/**
 * Třída reprezentuje políčko na hrací desce. Každé políčko je identifikováno pozicí [row, col],
 * která je nastavena při inicializaci v konstruktoru a dále se nemění (čísluje se od 1). 
 * Na políčko je možné umístit hrací kámen, viz. MazeStone
 * @see ija.game.board.MazeStone
 * @author Jan Herec, Pavel Juhaňák
 */
public class MazeField {
    private int row;
    private int col;
    private MazeStone maze_stone;
    
    /**
     * Konstruktor vytváří políčko na hrací desce
     * @param row číslo řádku, na kterém leží políčko na hrací desce 
     * @param col číslo sloupce, na kterém leží políčko na hrací desce 
     */
    public MazeField(int row, int col) {
        this.row = row;
        this.col = col;
        this.maze_stone = null;
    }
    
    /**
     * Vrací číslo řádku, na kterém je políčko umístěno na hrací desce
     * @return číslo řádku, na kterém je políčko umístěno na hrací desce
     */
    public int row() {
        return this.row;
    }
    
    /**
     * Vrací číslo sloupce, na kterém je políčko umístěno na hrací desce
     * @return číslo sloupce, na kterém je políčko umístěno na hrací desce
     */
    public int col() {
        return this.col;
    }
    
    /**
     * Vrací který se nachází na políčku
     * @return vrací kámen, který je umístěný na políčku. Pokud na políčku žádný kámen není, vrací null
     */
    public MazeStone getStone() {
        return this.maze_stone;
    }
    
    /**
     * Vloží hrací kámen na políčko
     * @param c hrací kámen, který budeme vkládat na políčko
     */
    public void putStone(MazeStone c) {
        this.maze_stone = c;
    }
}
