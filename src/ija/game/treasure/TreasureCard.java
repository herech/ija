/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu TreasureCard, viz dokumentační komentář této třídy 
 */

package ija.game.treasure;

/**
 * Třída reprezentuj kartu, která zobrazuje poklad
 * @see Treasure
 * @author Jan Herec, Pavel Juhaňák
 */
public class TreasureCard {

    protected Treasure treasure; // poklad který karta zobrazuje

    /**
     * Konstruktor který uloží poklad, který karta zobrazuje
     * @param tr zobrazovaný poklad
     */
    public TreasureCard(Treasure tr) {
        this.treasure = tr;
    }

    /**
    * Vrati poklad na karte ulozeny
    * @return poklad na karte ulozeny
    */
    public Treasure getTreasure() {
        return this.treasure;
    }

    /**
     * Metoda provádí overriding a testuje jestli se objket předaný jako argument rovná obsahově se současným objektem 
     * Tuto činnost deleguje na své složky
     * @param obj Objekt, který chceme obsahově porovnat se současným objektem
     * @return Vrací true pokud je objekt předaný jako argument obsahově shodný s tímto objektem
     */
    @Override
    public boolean equals(Object obj) {
        if ( !(obj instanceof TreasureCard) ) { return false; }

        return this.treasure.equals( ((TreasureCard)obj).treasure );
    }

    /**
     * Metoda provádí overriding a vrací celé číslo, které objekt identifikuje, tuto činnost deleguje na své složky 
     * @return Vrací číslo, které jednoznačně charakterizuje objekt
     */
    @Override
    public int hashCode() {
        return this.treasure.hashCode();
    }
}
