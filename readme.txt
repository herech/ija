Projekt do předmětu IJA 2014/2015
----------------------------------------------------------------------------------------------------------------------------
Autoři: Jan Herec, xherec00
        Pavel Juhaňák, xjuhan01
----------------------------------------------------------------------------------------------------------------------------
Kódování: UTF-8
----------------------------------------------------------------------------------------------------------------------------
Popis: - Hra Labyrint
       - Varianta A (implementována operace undo)
----------------------------------------------------------------------------------------------------------------------------
Vlastnosti hry: - Pěkná grafika
                - Dobrá hratelnost
                - Načítání a ukládání pozic do souborů v čitelném formátu XML - možnost ručně vytvářet vlastní pozici ve hře
                - Možnost volby velikosti okna v rámci nové hry

----------------------------------------------------------------------------------------------------------------------------       
Použité obrázky: - Obrázky karet, kamenů, pokladů jsou převzaty ze stejnojmenné deskové hry Labyrinth (byly naskenovány)
                 - Ostatní grafika byla vytvořena vlastnoručne, nebo byla převzata ze stránek, jež jí nabízí volně k užití





