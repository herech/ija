/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu GUI, viz dokumentační komentář této třídy 
 */
 
package ija.gui;

import ija.*;
import ija.game.*;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Random;
import java.io.File;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.*;
import java.nio.file.*;
import java.io.*;
import java.awt.Dialog;
import java.lang.StringBuffer;

/**
 * Třída GUI reprezentuje hlavní okno hry
 * @author Jan Herec, Pavel Juhaňák
 */
public class GUI extends JFrame {
    
    /**
     * Konstruktor, který vytvoří hlavní meny ve hře, tlačítko UNDO a hlavní JPanel, který bude obsahovat celou hru.
     * Definuje také obsluhu událostí při kliknutí na položky menu
     */
    public GUI() {
        
        // uložíme do třídy Game hlavní okno hry
        Game.setJFrameInstance(this);
        
        this.setTitle("Hra Labyrint");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Vytvoříme menu bar
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        // Definujeme UNDO a Hlavní menu v rámci menu baru
        JMenu mainMenu = new JMenu("Hlavní menu");
        JMenuItem undoAction = new JMenuItem("     UNDO");
        undoAction.setMaximumSize(new Dimension(95, 100));
        menuBar.add(mainMenu);
        menuBar.add(undoAction);
        
        // Vytvoříme polořky hlavního menu
        JMenuItem newGameAction = new JMenuItem("Nová hra");
        final JMenuItem saveGameAction = new JMenuItem("Uložit hru");
        saveGameAction.setEnabled(false); // zatím není možné ukládat hru, když žádnou nemáme
        JMenuItem loadGameAction = new JMenuItem("Načíst hru");
        JMenuItem exitAction = new JMenuItem("Konec");
        
        mainMenu.add(newGameAction);
        mainMenu.add(saveGameAction);
        mainMenu.add(loadGameAction);
        mainMenu.addSeparator();
        mainMenu.add(exitAction);

        // Definujeme obsluhu pro operaci undo
        undoAction.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                Command.undo();
            }
        });

        // Definujeme obsluhu pro volbu vytvoření nové hry
        newGameAction.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                // Vytvoříme nové okno a předáme mu saveGameAction, které následně aktivuje
                NewGameJFRame oknoNoveHry = new NewGameJFRame(saveGameAction);
                oknoNoveHry.setVisible(true);
            }
        });
        
        // Definujeme obsluhu pro volbu uložení hry
        saveGameAction.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                
                // Vytvoříme dialog pro uložení hry
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter xmlfilter = new FileNameExtensionFilter(".xml", "xml");
                fileChooser.setFileFilter(xmlfilter);
                fileChooser.setDialogTitle("Specifikujte název xml souboru, do kterého se uloží rozehraná hra"); 
                
                // nastavíme examples jako výchozí adresář pro ukládání hry
                File jarPath=new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
                String rootPath=jarPath.getParentFile().getParentFile().getAbsolutePath();
                fileChooser.setCurrentDirectory(new File(rootPath, "examples"));
                fileChooser.setSelectedFile(new File(new File(rootPath, "examples"), "moje_ulozena_pozice.xml"));
                 
                // Zobrazíme dialog pro uložení hry
                int userSelection = fileChooser.showSaveDialog(Game.getJFramelInstance());
                
                // POkud uživatel specifikoval soubor pro uložení hry, tak do tohoto souboru hru uložíme
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToSave = fileChooser.getSelectedFile();
                    
                    // Chyba při ukládání hry
                    if (XML.saveGameToXMLFile(fileToSave) == false) {
                        JOptionPane.showMessageDialog(Game.getJFramelInstance(),"Chyba, hru se nepodařilo uložit!", "Chyba", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });
        
        // Definujeme obsluhu pro volbu načtení hry
        loadGameAction.addActionListener(new ActionListener() {
            
            public void actionPerformed(ActionEvent arg0) {
                
                // Vytvoříme dialog pro načtení hry
                JFileChooser fileChooser = new JFileChooser();
                FileNameExtensionFilter xmlfilter = new FileNameExtensionFilter(".xml", "xml");
                fileChooser.setFileFilter(xmlfilter);
                fileChooser.setDialogTitle("Specifikujte název xml souboru, ze kterého se nahraje rozehraná hra"); 
                
                // nastavíme examples jako výchozí adresář pro načítání uložených pozic hry
                File jarPath=new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
                String rootPath=jarPath.getParentFile().getParentFile().getAbsolutePath();
                fileChooser.setCurrentDirectory(new File(rootPath, "examples"));
                
                // Zobrazíme dialog pro uložení hry
                int userSelection = fileChooser.showOpenDialog(Game.getJFramelInstance());
                
                // POkud uživatel specifikoval soubor pro uložení hry, tak do tohoto souboru hru uložíme
                if (userSelection == JFileChooser.APPROVE_OPTION) {
                    File fileToRead = fileChooser.getSelectedFile();
                    
                    // Chyba při ukládání hry
                    if (XML.loadGameFromXMLFile(fileToRead) == false) {
                        JOptionPane.showMessageDialog(Game.getJFramelInstance(),"Chyba, hru se nepodařilo načíst!", "Chyba", JOptionPane.ERROR_MESSAGE);
                    }
                }
                
                // nakonec po načtení hry povolíme uložení hry
                saveGameAction.setEnabled(true);
            }
        });
        
        // Definujeme obsluhu pro volbu ukončení hry (hru ukončíme)
        exitAction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                System.exit(0);
            }
        });
        
        // přidáme pro naše hlavní okno také hlavní JPanel
        GuiJPanel main_panel = new GuiJPanel();

        this.add(main_panel);

        this.pack();
    }

}