/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu Player, viz dokumentační komentář této třídy 
 */
 
package ija.game;

import ija.game.board.*;
import ija.game.treasure.*;
import ija.gui.*;
import java.util.*;
import javax.swing.*;
import java.awt.*;

/**
 * Třída Player reprezentuje hráče ve hře
 * @author Jan Herec, Pavel Juhaňák
 */

public class Player {
  
    /** Karta s pokladem který hráč hledá */
    private TreasureCard card = null;

    /** Otočené karty */
    private ArrayList<Integer> turned_cards = new ArrayList<Integer>(); 
    
    /** index poslední otoené karty */
    private int index_of_last_turned_card;
    
    /** řádek na kterém se hráč nachází */
    private int row;
    /** sloupec na kterém se hráč nachází */
    private int col;

    /**
     * Konstruktor inicializuje instanci hráče
     */
    public Player() {
        for (int i = 0; i < Game.getNumberOfTreasures() / Game.getNumberOfPlayers(); i++) {
            this.turned_cards.add(25);
        }
        this.index_of_last_turned_card = 0;
    }

    /**
     * Konstruktor který pouze vytvoří instanci
     * @param do_nothing jenom kvůli přetížení
     */
    public Player(boolean do_nothing) {

    }

    /**
     * Nastaví na jakém řádku se hráč nachází
     * @param row řádek na kterém se hráč nachází
     */
    public void setRow(int row) {
        this.row = row;
    }

    /**
     * Nastaví na jakém sloupci se hráč nachází
     * @param col sloupec an kterém se hráč nachází
     */
    public void setCol(int col) {
        this.col = col;
    }

    /**
     * Vrací řádek na kterém je hráč
     * @return řádek na kterém je hráč
     */
    public int getRow() {
        return this.row;
    }

    /**
     * Vrací sloupec na kterém je hráč
     * @return sloupec na kterém je hráč
     */
    public int getCol() {
        return this.col;
    }

    /**
     * Vrací počet nalezených pokladů
     * @return počet nalezených pokladů
     */
    public int getNumOfTreasures() {
        return this.index_of_last_turned_card + 1;
    }

    /**
     * Nastaví aktuálně hledanou kartu
     * @param card hledaná karta
     */
    public void setTreasureCard(TreasureCard card) {
        this.card = card;
    }

    /**
     * Vrací aktuálně hledanou kartu
     * @return this.card aktuálně hledaná karta
     */
    public TreasureCard getTreasureCard() {
        return this.card;
    }
    
    /**
    * Získá otočenou kartu hráče - může to být i neotočená karta
    * @param index otočené karty
    * @return Obrázek otočené karty 
    */
    public Image getTurnedCard(int index) {
        return Game.getCards(this.turned_cards.get(index));
    }

    /**
    * Odebere hráči poslední otočenou kartu
    */
    public void removeTurnedCard() {
        this.index_of_last_turned_card--;
        this.turned_cards.set(this.index_of_last_turned_card, 25);
    }

    /**
    * Přidá otočenou kartu k hráči
    * @param treasure_code kód pokladu na kartě
    */
    public void addTurnedCard(int treasure_code) {
        this.turned_cards.set(this.index_of_last_turned_card, treasure_code);
        this.index_of_last_turned_card++;
    }
    
    /**
    * Přidá otočenou kartu k hráči - použito při načítání hry
    * @param treasure_code kód pokladu na kartě
    * @param without_index jenom kvůli přetížení
    */
    public void addTurnedCard(int treasure_code, boolean without_index) {
        this.turned_cards.add(treasure_code);
    }
    
    /**
    * Vrátí otočené karty
    * @return otočené karty
    */
    public ArrayList<Integer> getTurnedCards() {
        return this.turned_cards;
    }

    /**
    * Vrátí index poslední přidané karty
    * @return this.index_of_last_turned_card
    */
    public int getIndexOfLastTurnedCard() {
        return this.index_of_last_turned_card;
    }

    /**
     * Nastaví index poslední přidané karty
     * @param index index poslední přidané karty
     */
    public void setIndexOfLastTurnedCard(int index) {
        this.index_of_last_turned_card = index;
    }
}
