/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu Treasure, viz dokumentační komentář této třídy 
 */

package ija.game.treasure;

/**
 * Třída reprezentuj poklad
 * @author Jan Herec, Pavel Juhaňák
 */
public class Treasure {

    private int code;                                              // kód který jednoznačně identifikuje poklad
    private static Treasure[] setOfTreasures = new Treasure[25];   // pole obsahuje vygenerovaný set pokladů

    /**
     * Soukromý konstruktor který nastaví kód pokladu (volá se pouze z metody createSet)
     * @param code kód který jednoznačně identifikuje poklad
     */
    private Treasure(int code) {
        this.code = code;
    }

    /**
     * Statická metoda vytváří set pokladů (do pole ukládá vytvořené instance objektů třídy Treasure)
     */
    public static void createSet() {
        for(int i = 0; i < setOfTreasures.length; i++) {
            setOfTreasures[i] = new Treasure(i);
        }
    }

    /**
     * Metoda vrací na základě kódu daný poklad, který je tímto kódem identifikován
     * @param code kód který jednoznačně identifikuje poklad
     * @return Vrací na základě kódu daný poklad, který je tímto kódem identifikován, nebo null, pokud poklad s daným kódem neexistuje
     */
    public static Treasure getTreasure(int code) {
        return (code > setOfTreasures.length) ? null : setOfTreasures[code];
    }

    /**
    * Metoda vrací číslený kód pokladu
    * @return číslený kód pokladu
    */
    public int getTreasureCode() {
        return this.code;
    }

    /**
     * Metoda provádí overriding a testuje jestli se objekt předaný jako argument rovná obsahově se současným objektem 
     * To provádí na základě porovnání kódů pokladu
     * @param o Objekt, který chceme obsahově porovnat se současným objektem
     * @return Vrací true pokud je objekt předaný jako argument obsahově shodný s tímto objektem
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Treasure) {
            Treasure card = (Treasure) o;
            if (this.code == card.code) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    /**
     * Metoda provádí overriding a vrací celé číslo, které objekt identifikuje (kód pokladu)
     * @return Vrací číslo, které jednoznačně charakterizuje objekt (kód pokladu)
     */
    @Override
    public int hashCode() {
        return this.code;
    }
}
