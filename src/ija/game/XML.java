/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu XML, viz dokumentační komentář této třídy 
 */
 
package ija.game;

import ija.game.board.*;
import ija.game.treasure.*;
import ija.gui.*;
import java.util.*;
import ija.*;
import java.io.File;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;

/**
 * Třída XML slouží pro uložení současné hry do souboru xml a také pro načtení již uložené hry
 * @author Jan Herec, Pavel Juhaňák
 */

public class XML {
    
    /** 
     * Metoda uloží současnou hru do souboru XML
     * @param file xml soubor kam se bude hra ukládat
     * @return true nebo false v zavislosti na tom jestli nastala chyba
     */
    public static boolean saveGameToXMLFile(File file) {
        try {
 
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
     
            // vytvoříme xml a do nej uložíme root element
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("game");
            doc.appendChild(rootElement);
     
            // uložíme šířka okna
            Element window_width = doc.createElement("window_width");
            window_width.appendChild(doc.createTextNode(String.valueOf(Game.getWindowWidth())));
            rootElement.appendChild(window_width);
            
            // uložíme výška okna
            Element window_height = doc.createElement("window_height");
            window_height.appendChild(doc.createTextNode(String.valueOf(Game.getWindowHeight())));
            rootElement.appendChild(window_height);
            
            // uložíme počet hráčů
            Element number_of_players = doc.createElement("number_of_players");
            number_of_players.appendChild(doc.createTextNode(String.valueOf(Game.getNumberOfPlayers())));
            rootElement.appendChild(number_of_players);
            
            // uložíme počet pokladů
            Element number_of_treasures = doc.createElement("number_of_treasures");
            number_of_treasures.appendChild(doc.createTextNode(String.valueOf(Game.getNumberOfTreasures())));
            rootElement.appendChild(number_of_treasures);
            
            // uložíme velikost desky
            Element size_of_board = doc.createElement("size_of_board");
            size_of_board.appendChild(doc.createTextNode(String.valueOf(Game.getSizeOfBoard())));
            rootElement.appendChild(size_of_board);
            
            // uložíme stavové proměnné hry
            Element can_interact = doc.createElement("can_interact");
            can_interact.appendChild(doc.createTextNode(String.valueOf(Game.getCanInteract())));
            rootElement.appendChild(can_interact);
            
            Element last_shift_row = doc.createElement("last_shift_row");
            last_shift_row.appendChild(doc.createTextNode(String.valueOf(Game.getLastShiftRow())));
            rootElement.appendChild(last_shift_row);
            
            Element last_shift_col = doc.createElement("last_shift_col");
            last_shift_col.appendChild(doc.createTextNode(String.valueOf(Game.getLastShiftCol())));
            rootElement.appendChild(last_shift_col);
            
            Element player_did_shift = doc.createElement("player_did_shift");
            player_did_shift.appendChild(doc.createTextNode(String.valueOf(Game.getPlayerDidShift())));
            rootElement.appendChild(player_did_shift);
            
            Element current_player = doc.createElement("current_player");
            current_player.appendChild(doc.createTextNode(String.valueOf(Game.getIndexCurrenPlayer())));
            rootElement.appendChild(current_player);
            
            Element winner = doc.createElement("winner");
            winner.appendChild(doc.createTextNode(String.valueOf(Game.isWinner())));
            rootElement.appendChild(winner);
            
            Element can_shift = doc.createElement("can_shift");
            can_shift.appendChild(doc.createTextNode(String.valueOf(Game.getCanShift())));
            rootElement.appendChild(can_shift);
            
            // uložíme hráče
            Element players = doc.createElement("players");
            rootElement.appendChild(players);
            
            for (int i = 0; i < Game.getNumberOfPlayers(); i++) {
                // hráč
                Element player = doc.createElement("player");
                players.appendChild(player);
                
                Player pl = Game.getPlayer(i);
                
                // uložíme řádek na kterém je hráč
                Element row = doc.createElement("row");
                row.appendChild(doc.createTextNode(String.valueOf(pl.getRow())));
                player.appendChild(row);
                
                // uložíme sloupec na kterém je hráč
                Element col = doc.createElement("col");
                col.appendChild(doc.createTextNode(String.valueOf(pl.getCol())));
                player.appendChild(col);
                
                // uložíme kód pokladu, jež otočená karta zobrazuje
                int treasureCode = 0;
                if (pl.getTreasureCard() != null) {
                    treasureCode = pl.getTreasureCard().getTreasure().getTreasureCode();
                }
                Element treasure_code = doc.createElement("treasure_code");
                treasure_code.appendChild(doc.createTextNode(String.valueOf(treasureCode)));
                player.appendChild(treasure_code);
                
                // uložíme index poslední otočené karty
                Element index_of_last_turned_card = doc.createElement("index_of_last_turned_card");
                index_of_last_turned_card.appendChild(doc.createTextNode(String.valueOf(pl.getIndexOfLastTurnedCard())));
                player.appendChild(index_of_last_turned_card);
                
                Element turned_cards = doc.createElement("turned_cards");
                player.appendChild(turned_cards);

                // uložíme karty otočené a neotočené
                for (Integer item : pl.getTurnedCards()) {
                    Element turned_card = doc.createElement("turned_card");
                    turned_card.appendChild(doc.createTextNode(String.valueOf(item)));
                    turned_cards.appendChild(turned_card);
                }
            }
            
            // uložíme balíček karet
            Element package_of_threasure_cards = doc.createElement("package_of_threasure_cards");
            rootElement.appendChild(package_of_threasure_cards);
            
            // uložíme počet karet v balíčku
            Element act_size_of_package = doc.createElement("act_size_of_package");
            act_size_of_package.appendChild(doc.createTextNode(String.valueOf(Game.getCardPack().getActSize())));
            package_of_threasure_cards.appendChild(act_size_of_package);
            
            // uložíme karty balíčku
            for (TreasureCard item : Game.getCardPack().getPackageOfTReasureCards()) {
                Element threasure_card_code = doc.createElement("threasure_card_code");
                threasure_card_code.appendChild(doc.createTextNode(String.valueOf(item.getTreasure().getTreasureCode())));
                package_of_threasure_cards.appendChild(threasure_card_code);
            }
     
            // uložíme desku
            Element board = doc.createElement("board");
            rootElement.appendChild(board);
            
            // uložíme volný kámen
            Element free_stone = doc.createElement("free_stone");
            board.appendChild(free_stone);
            
            Element free_stone_type = doc.createElement("free_stone_type");
            free_stone_type.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().getFreeStone().getType())));
            free_stone.appendChild(free_stone_type);
            
            Element free_stone_rotation = doc.createElement("free_stone_rotation");
            free_stone_rotation.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().getFreeStone().getRotation())));
            free_stone.appendChild(free_stone_rotation);
            
            Element free_stone_treasure_code = doc.createElement("free_stone_treasure_code");
            free_stone_treasure_code.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().getFreeStone().getTreasure().getTreasureCode())));
            free_stone.appendChild(free_stone_treasure_code);
            
            // uložíme kameny na políčkách
            
            Element stones = doc.createElement("stones");
            board.appendChild(stones);
            
            for (int i = 0; i < Game.getSizeOfBoard() * Game.getSizeOfBoard(); i++) {
                
                int row = i / Game.getSizeOfBoard();
                int col = i % Game.getSizeOfBoard();
                
                Element stone = doc.createElement("stone");
                stones.appendChild(stone);

                Element stone_type = doc.createElement("stone_type");
                stone_type.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().get(row, col).getStone().getType())));
                stone.appendChild(stone_type);
            
                Element stone_rotation = doc.createElement("stone_rotation");
                stone_rotation.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().get(row, col).getStone().getRotation())));
                stone.appendChild(stone_rotation);
            
                Element stone_treasure_code = doc.createElement("stone_treasure_code");
                stone_treasure_code.appendChild(doc.createTextNode(String.valueOf(Game.getBoard().get(row, col).getStone().getTreasure().getTreasureCode())));
                stone.appendChild(stone_treasure_code);
            }
     
            // zapíšeme xml do souboru
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);
     
            transformer.transform(source, result);
     
        } 
        // chyba při ukládání souboru
        catch (Exception exception) {
            return false;
        }
        
        return true;

    }
    
    /** 
     * Metoda načte hru ze souboru XML
     * @param file xml soubor ze kterého se bude hra načítat
     * @return true nebo false v zavislosti na tom jestli nastala chyba
     */
    public static boolean loadGameFromXMLFile(File file) {
        try {
            // získáme xml soubor
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
    
            doc.getDocumentElement().normalize();
         
            // zjistíme a nastavíme velikost okna
            Game.setWindowWidth(Integer.valueOf(doc.getElementsByTagName("window_width").item(0).getTextContent()));
            Game.setWindowHeight(Integer.valueOf(doc.getElementsByTagName("window_height").item(0).getTextContent()));
            
            // zjistíme a nastavíme počet hráčů
            Game.setNumberOfPlayers(Integer.valueOf(doc.getElementsByTagName("number_of_players").item(0).getTextContent()));
            
            // zjistíme a nastavíme počet pokladů
            Game.setNumberOfTreasures(Integer.valueOf(doc.getElementsByTagName("number_of_treasures").item(0).getTextContent()));
            
            // zjistíme a nastavíme velikost hrací desky
            Game.setSizeOfBoard(Integer.valueOf(doc.getElementsByTagName("size_of_board").item(0).getTextContent()));
            
            // zjistíme a nastavujeme stavové proměnné hry
            Game.setCanInteract(Boolean.valueOf(doc.getElementsByTagName("can_interact").item(0).getTextContent()));
            
            Game.setLastShiftRow(Integer.valueOf(doc.getElementsByTagName("last_shift_row").item(0).getTextContent()));
            
            Game.setLastShiftCol(Integer.valueOf(doc.getElementsByTagName("last_shift_col").item(0).getTextContent()));
            
            Game.setPlayerDidShift(Boolean.valueOf(doc.getElementsByTagName("player_did_shift").item(0).getTextContent()));

            Game.setIndexCurrenPlayer(Integer.valueOf(doc.getElementsByTagName("current_player").item(0).getTextContent()));
            
            Game.setIsWinner(Boolean.valueOf(doc.getElementsByTagName("winner").item(0).getTextContent()));
            
            Game.setCanShift(Boolean.valueOf(doc.getElementsByTagName("can_shift").item(0).getTextContent()));
         
            // zjistíme a vytvoříme set pokladů
            Treasure.createSet();
         
            // zjistíme a  nastavíme hráče hry
            NodeList player_list = doc.getElementsByTagName("player");
            for (int i = 0; i < player_list.getLength(); i++) {
                Node node = player_list.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    
                    Player pl = new Player(true);
                    
                    int row = Integer.valueOf( ((Element) node).getElementsByTagName("row").item(0).getTextContent());
                    int col = Integer.valueOf( ((Element) node).getElementsByTagName("col").item(0).getTextContent());
                    int treasure_code = Integer.valueOf( ((Element) node).getElementsByTagName("treasure_code").item(0).getTextContent());
                    int index_of_last_turned_card = Integer.valueOf( ((Element) node).getElementsByTagName("index_of_last_turned_card").item(0).getTextContent()); 
                    
                    pl.setRow(row);
                    pl.setCol(col);
                    pl.setIndexOfLastTurnedCard(index_of_last_turned_card);
                    
                    // zjistíme a nastavíme aktuální kartu hráče, která zobrazuje heldaný poklad
                    if (treasure_code != 0) {
                        TreasureCard new_treasure_card = new TreasureCard(Treasure.getTreasure(treasure_code));
                        pl.setTreasureCard(new_treasure_card);
                    }
                    
                    // zjistíme a nastavíme otočené karty v rámci hráče
                    NodeList turned_cards_list = ((Element) node).getElementsByTagName("turned_card");
                    for (int j = 0; j < turned_cards_list.getLength(); j++) {
                        pl.addTurnedCard(Integer.valueOf(turned_cards_list.item(j).getTextContent()), true);
                    }
                    Game.setPlayer(i, pl);
                }
            }
            
            // zjistíme a nastavíme balíček karet (s poklady)
            CardPack card_pack_inst = new CardPack();
            card_pack_inst.setActSize(Integer.valueOf(doc.getElementsByTagName("act_size_of_package").item(0).getTextContent()));
            
            NodeList card_package_list = doc.getElementsByTagName("threasure_card_code");
            for (int i = 0; i < card_package_list.getLength(); i++) {
                card_pack_inst.pushCardToPack(Integer.valueOf(card_package_list.item(i).getTextContent()));
            }
            
            Game.setCardPack(card_pack_inst);
            
            // zjistíme a nastavíme desku
            Game.setBoard(MazeBoard.createMazeBoard(Game.getSizeOfBoard()));
            
            // zjistíme a nastavíme volný kámen
            String free_stone_name = doc.getElementsByTagName("free_stone_type").item(0).getTextContent();
            MazeStone free_maze_stone_inst = MazeStone.create(free_stone_name);
            free_maze_stone_inst.putTreasure( Treasure.getTreasure(Integer.valueOf(doc.getElementsByTagName("free_stone_treasure_code").item(0).getTextContent())));
            
            int rotation_free = Integer.valueOf(doc.getElementsByTagName("free_stone_rotation").item(0).getTextContent());
            for (int k = 0; k < rotation_free / 90; k++) {
                free_maze_stone_inst.turnRight();
            } 
            free_maze_stone_inst.setRotation(rotation_free);
            Game.getBoard().putFreeStone(free_maze_stone_inst);
            
            // zjistíme a  nastavíme kameny a poklady na políčkách
            NodeList stone_list = doc.getElementsByTagName("stone");
            for (int i = 0; i < stone_list.getLength(); i++) {
                Node node3 = stone_list.item(i);
                if (node3.getNodeType() == Node.ELEMENT_NODE) {
                    
                    int r = i / Game.getSizeOfBoard();
                    int c = i % Game.getSizeOfBoard();
                    
                    String stone_name = ((Element) node3).getElementsByTagName("stone_type").item(0).getTextContent();
                    MazeStone maze_stone_inst = MazeStone.create(stone_name);
        
                    maze_stone_inst.putTreasure( Treasure.getTreasure(Integer.valueOf(((Element) node3).getElementsByTagName("stone_treasure_code").item(0).getTextContent())));
                    int rotation = Integer.valueOf(((Element) node3).getElementsByTagName("stone_rotation").item(0).getTextContent());
                    for (int k = 0; k < rotation / 90; k++) {
                        maze_stone_inst.turnRight();
                    } 
                    maze_stone_inst.setRotation(rotation);
            
                    Game.getBoard().putStone(r, c, maze_stone_inst);
                
                }
            }
            
            // vyresetujeme pole kamenů a karet otočených
            Game.clearButtonStones();
            Game.clearLabelTurnedCards();
            
            // vytvoříme komponenty a vykreslíme je
            Game.getJPanelInstance().removeAll();
            Game.getJPanelInstance().createBoard();
            Game.getJPanelInstance().redrawBoard();
            Game.getJPanelInstance().redrawTurnedCards();
 
        } 
        // chyba při načítání hry
        catch (Exception exception) {
            return false;
        }
        Command.clear(); // vyprázdníme frontu undo
        return true;

    }
}
