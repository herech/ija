/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu Command, viz dokumentační komentář této třídy
 */
 
package ija;

import ija.game.*;
import ija.game.board.*;
import ija.game.treasure.*;
import ija.gui.*;

import java.util.*;

/**
 * Třída Command implementuje návrhový vzor Command, v tomto případě slouží pro provádění undo operace
 * @author Jan Herec, Pavel Juhaňák
 */

public class Command {
    /**
     * Hrac se posune na nake pole, ale nic jineho se nedeje
     * ulozi se:
     * - index posouvaneho hrace
     * - pozice hrace pred presunem
     *
     * undo udela:
     * - pozice hrace se nastavi na pozici pred posunem
     */
    private static final int MOVE_PLAYER = 0;

    /**
     * Posunou se kameny nekterou z sipek
     * ulozi se:
     * - souradnice naproti mistu kam byl kamen vsunut
     * - rotace vysunuteho kamene (ted je z nej freeStone)
     * - indexy a puvodni pozice vsech hracu kteri stali na posouvanem radku/sloupci
     *                                      (nebo klidne vsech, stejne sou jenom 4)
     *
     * undo udela:
     * - natoci volny kamen na puvodni rotaci
     * - vlozi volny kamen na misto urcene souradnicemi (na druhou stranu nez byl puvodni tah)
     * - zde specifikovanym hracum se nastavi pozice na pozici pred posunem
     */
    private static final int MOVE_STONES = 1;

    /**
     * Hrac se posune na nake pole, vezme poklad (odstrani ho z desky),
     *    kartu soucasneho pokladu si da k otocenym a vytahne si novou kartu
     * ulozi se:
     * - index posouvaneho hrace
     * - pozice hrace pred presunem
     * - pozice kamenu na ktery sel
     * - kod pokladu
     *
     * undo udela:
     * - pozice hrace se nastavi na pozici pred posunem
     * - na pozici kamene se prida poklad
     * - na vrch balicku karet se vrati obdrzena karta
     * - vrchni karta v ziskanych kartach se vlozi na misto hledane karty
     * - odstrani se vrchni karta ze ziskanych karet a snizi se index
     */
    private static final int MOVE_PLAYER_AND_TAKE_TREASURE = 2;

    /** 
     * Typ akce, kterou budeme ukládat a poté vracet
     */
    private int type;

    /**
     * Promenne pro rozpoznani stavu hry
     */
    private boolean can_interact;
    private int last_shift_row;
    private int last_shift_col;
    private boolean player_did_shift;
    private int current_player;
    private boolean winner;
    private boolean can_shift;

    private int[][] players;
    private int top_player_index;
    private static final int PLAYER_INDEX = 0;
    private static final int PLAYER_ROW = 1;
    private static final int PLAYER_COL = 2;

    private int stone_row;
    private int stone_col;
    private int stone_rotation;

    private int treasure;

    /**
     * Sem se budou ukladat jenotlive prikazy
     */
    private static Stack<Command> stack = new Stack<Command>();

    /**
     * Zachytíme typ akce, která představuje pohyb hráče po desce bez sebrání pokladu
     * @param can_interact příznak jestli může hráč s hrou interagovat
     * @param last_shift_row řádek ve kterém se prováděl posun kamenů
     * @param last_shift_col sloupec ve kterém se prováděl posun kamenů
     * @param player_did_shift příznak jestli hráč dělal posun kamenů na desce
     * @param current_player index do pole hráčů na aktuálního hráče
     * @param winner je aktuální hráč vítěz
     * @param can_shift může aktuální hráč provádět posun kamenů na desce
     * @param player pole hráčů
     */
    public static void capture(boolean can_interact, int last_shift_row, int last_shift_col,
                               boolean player_did_shift, int current_player, boolean winner, boolean can_shift,
                               int[] player) {
        
        Command command = new Command();

        command.type = MOVE_PLAYER;

        command.can_interact = can_interact;
        command.last_shift_row = last_shift_row;
        command.last_shift_col = last_shift_col;
        command.player_did_shift = player_did_shift;
        command.current_player = current_player;
        command.winner = winner;
        command.can_shift = can_shift;

        command.players = new int[1][3];
        command.top_player_index = 0;

        command.players[0][0] = player[0];
        command.players[0][1] = player[1];
        command.players[0][2] = player[2];

        Command.stack.push(command);
    }

    /**
     * Zachytíme typ akce, která představuje posun kamene na desce
     * @param can_interact příznak jestli může hráč s hrou interagovat
     * @param last_shift_row řádek ve kterém se prováděl posun kamenů
     * @param last_shift_col sloupec ve kterém se prováděl posun kamenů
     * @param player_did_shift příznak jestli hráč dělal posun kamenů na desce
     * @param current_player index do pole hráčů na aktuálního hráče
     * @param winner je aktuální hráč vítěz
     * @param can_shift může aktuální hráč provádět posun kamenů na desce
     * @param s_row řádek na kterém se hráč nachází
     * @param s_col sloupec na kterém se hráč nachází
     * @param rotation natočení volného kamene
     * @param players pole hráčů
     */
    public static void capture(boolean can_interact, int last_shift_row, int last_shift_col,
                               boolean player_did_shift, int current_player, boolean winner, boolean can_shift,
                               int s_row, int s_col, int rotation, int[][] players) {
        Command command = new Command();

        command.type = MOVE_STONES;

        command.can_interact = can_interact;
        command.last_shift_row = last_shift_row;
        command.last_shift_col = last_shift_col;
        command.player_did_shift = player_did_shift;
        command.current_player = current_player;
        command.winner = winner;
        command.can_shift = can_shift;

        if (s_row == 0 || s_row == Game.getSizeOfBoard() - 1) {
            command.stone_row = (Game.getSizeOfBoard() - 1) - s_row;
            command.stone_col = s_col;
        } else if (s_col == 0 || s_col == Game.getSizeOfBoard() - 1) {
            command.stone_row = s_row;
            command.stone_col = (Game.getSizeOfBoard() - 1) - s_col;
        }

        command.stone_rotation = rotation;
        command.players = new int[players.length][3];

        for (int i = 0; i < players.length; i++) {
            command.top_player_index = i;
            for (int j = 0; j < 3; j++) {
                command.players[i][j] = players[i][j];
            }
        }

        Command.stack.push(command);
    }

    /**
     * Zachytíme typ akce, která představuje pohyb hráče po desce kdy hráč vezme poklad
     * @param can_interact příznak jestli může hráč s hrou interagovat
     * @param last_shift_row řádek ve kterém se prováděl posun kamenů
     * @param last_shift_col sloupec ve kterém se prováděl posun kamenů
     * @param player_did_shift příznak jestli hráč dělal posun kamenů na desce
     * @param current_player index do pole hráčů na aktuálního hráče
     * @param winner je aktuální hráč vítěz
     * @param can_shift může aktuální hráč provádět posun kamenů na desce
     * @param player pole hráčů
     * @param s_row řádek na kterém se hráč nachází
     * @param s_col sloupec na kterém se hráč nachází
     * @param treasure poklad který hráč vzal
     */
    public static void capture(boolean can_interact, int last_shift_row, int last_shift_col,
                               boolean player_did_shift, int current_player, boolean winner, boolean can_shift,
                               int[] player, int s_row, int s_col, int treasure) {
        Command command = new Command();

        command.type = MOVE_PLAYER_AND_TAKE_TREASURE;

        command.can_interact = can_interact;
        command.last_shift_row = last_shift_row;
        command.last_shift_col = last_shift_col;
        command.player_did_shift = player_did_shift;
        command.current_player = current_player;
        command.winner = winner;
        command.can_shift = can_shift;

        command.players = new int[1][3];
        command.top_player_index = 0;

        command.players[0][0] = player[0];
        command.players[0][1] = player[1];
        command.players[0][2] = player[2];

        command.stone_row = s_row;
        command.stone_col = s_col;

        command.treasure = treasure;

        Command.stack.push(command);
    }

    /**
     * Vytahne vrchni prikaz ze zasobniku operaci/akci a zvrati ho
     */
    public static void undo() {
        if (Command.stack.size() > 0) {
            Command command = Command.stack.pop();

            Game.setCanInteract(command.can_interact);
            Game.setLastShiftRow(command.last_shift_row);
            Game.setLastShiftCol(command.last_shift_col);
            Game.setPlayerDidShift(command.player_did_shift);
            Game.setIndexCurrenPlayer(command.current_player);
            Game.setIsWinner(command.winner);
            Game.setCanShift(command.can_shift);

            if (command.type == MOVE_PLAYER) {
                int act_row = Game.getPlayer(command.players[0][PLAYER_INDEX]).getRow();
                int act_col = Game.getPlayer(command.players[0][PLAYER_INDEX]).getCol();

                Game.getPlayer(command.players[0][PLAYER_INDEX]).setRow(command.players[0][PLAYER_ROW]);
                Game.getPlayer(command.players[0][PLAYER_INDEX]).setCol(command.players[0][PLAYER_COL]);

                GuiJPanel.setLabelWhoPlayOrWin();
                GuiJPanel.setColorOfLabelPlayer();
                Game.getJPanelInstance().redrawActualCard();
                Game.getJPanelInstance().redrawTurnedCards();

                Game.getJPanelInstance().redrawStone(act_row, act_col);
                Game.getJPanelInstance().redrawStone(command.players[0][PLAYER_ROW], command.players[0][PLAYER_COL]);
                
                Game.getJPanelInstance().repaint();

            } else if (command.type == MOVE_STONES) {
                MazeField field = new MazeField(command.stone_row, command.stone_col);

                int rot = command.stone_rotation - Game.getBoard().getFreeStone().getRotation();
                rot = rot < 0 ? 360 + rot : rot;
                while (rot > 0) {
                    rot -= 90;
                    Game.getBoard().getFreeStone().turnRight();
                }

                Game.getBoard().shift(field);

                for (int i = 0; i <= command.top_player_index; i++) {
                    Game.getPlayer(command.players[i][PLAYER_INDEX]).setRow(command.players[i][PLAYER_ROW]);
                    Game.getPlayer(command.players[i][PLAYER_INDEX]).setCol(command.players[i][PLAYER_COL]);
                }

                if (command.stone_row == 0 || command.stone_row == Game.getSizeOfBoard() - 1) {
                    Game.getJPanelInstance().redrawCol(command.stone_col);
                } else if (command.stone_col == 0 || command.stone_col == Game.getSizeOfBoard() - 1) {
                    Game.getJPanelInstance().redrawRow(command.stone_row);
                }

            } else if (command.type == MOVE_PLAYER_AND_TAKE_TREASURE) {
                int act_row = Game.getPlayer(command.players[0][PLAYER_INDEX]).getRow();
                int act_col = Game.getPlayer(command.players[0][PLAYER_INDEX]).getCol();

                Game.getPlayer(command.players[0][PLAYER_INDEX]).setRow(command.players[0][PLAYER_ROW]);
                Game.getPlayer(command.players[0][PLAYER_INDEX]).setCol(command.players[0][PLAYER_COL]);

                // Vrati poklad na pozici
                Game.getBoard().get(command.stone_row, command.stone_col).getStone().putTreasure( Treasure.getTreasure(command.treasure));

                // Vrati aktualne hledanou kartu na vrchol balicku
                Game.getCardPack().pushCardToPack(Game.getPlayer(command.players[0][PLAYER_INDEX]).getTreasureCard().getTreasure().getTreasureCode());

                // Hraci se nastavi predchozi hledana karta
                Game.getPlayer(command.players[0][PLAYER_INDEX]).setTreasureCard( new TreasureCard( Treasure.getTreasure(command.treasure) ));

                // U hrace se odstrani vrchni ziskana karta
                Game.getPlayer(command.players[0][PLAYER_INDEX]).removeTurnedCard();

                GuiJPanel.setLabelWhoPlayOrWin();
                GuiJPanel.setColorOfLabelPlayer();
                Game.getJPanelInstance().redrawActualCard();
                Game.getJPanelInstance().redrawTurnedCards();

                Game.getJPanelInstance().redrawStone(act_row, act_col);
                Game.getJPanelInstance().redrawStone(command.players[0][PLAYER_ROW], command.players[0][PLAYER_COL]);

                Game.getJPanelInstance().repaint();
            }
        }
    }

    /**
     * Vyčistíme zásobník příkazů
     */
    public static void clear() {
        Command.stack.clear();
    }

}