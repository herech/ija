/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu GuiJPanel, viz dokumentační komentář této třídy 
 */

package ija.gui;

import ija.*;
import ija.game.*;

import javax.swing.*;
import java.awt.*;
import java.util.Random;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.awt.image.AffineTransformOp ;
import java.lang.*;
import java.lang.Object.*;
import java.lang.Math.*;
import java.awt.geom.AffineTransform;
import javax.imageio.*;
import java.io.*;
import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;

/**
 * Třída GuiJPanel reprezentuje hlavní JPanel hry, obsahuje metody pro vytvoření a vykreslení komponent hry a pomocné metody pro tyto účely
 * @author Jan Herec, Pavel Juhaňák
 */

public class GuiJPanel extends JPanel {
    
    /**
     * Velikosti některých komponent, které se používají na více místech
     */
    private static int right_panel_width;
    private static int turned_card_width;
    private static int turned_card_height;
    
    /**
     * Některé komponenty, které se používají na více místech
     */
    private static JLabel label_who_play_name;    
    private static JLabel label_who_play_or_win;    
    private static JLabel actual_card;

    /**
     * Konstruktor nastaví základn parametry JPanelu jako velikost, typ Layout Manageru a umístění
     */
    public GuiJPanel() {
    
        this.setPreferredSize(new Dimension(Game.getWindowWidth(), Game.getWindowHeight()));
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        
        // do třídy Game si uložíme instanci hlavního JPanelu
        Game.setJPanelInstance(this);
        
    }
    
    /**
     * Metoda pro vytvoření komponent hry a přidání obsluhy akcí k těmto komponentám
     */
    public void createBoard() {
        Game.getJPanelInstance().setPreferredSize(new Dimension(Game.getWindowWidth(), Game.getWindowHeight()));
        int count = Game.getSizeOfBoard(); // velikost hrací desky (počet kamenů na řádku a sloupci)
        int gap = 2; // mezera mezi kameny
        
        // vypočítáme velikost kamene v závislosti na velikost okna a velikost hrací desky
        float scale = (11.0f / (float) (count + 2)) * 0.09f;
        int w = (int) (scale * Game.getWindowHeight()) - gap;
        int h = (int) (scale * Game.getWindowHeight()) - gap;
        
        Game.setWidthStone(w);
        Game.setHeightStone(h);
        
        // panel který bude obalovat hrací desku + šipky pro posun
        JPanel board_panel = new JPanel();
        board_panel.setOpaque(true);
        board_panel.setBackground(new Color(0,0,0,0));
        board_panel.setPreferredSize(new Dimension((count + 2) * (w + gap), (count + 2) * (h + gap)));
        board_panel.setLayout(new GridLayout(count + 2, count + 2, gap, gap));
        
        // kvůli optimalizaci si obrázky změnšíme a předrotujeme jedenkrát
        
        // otočení o 90° pro normální šipku
        
        Image img = Game.getArrow();

        img = rotateImage(img, 90);
        img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_90 = new ImageIcon( img );
        
        // otočení o 90° pro hover šipku
        
        Image img_hover = Game.getArrowHover();

        img_hover = rotateImage(img_hover, 90);
        img_hover = img_hover.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_90_hover = new ImageIcon( img_hover );
        
        // --------------------------------------------------
        
        // otočení o 270° pro normální šipku
        
        img = Game.getArrow();

        img = rotateImage(img, 270);
        img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_270 = new ImageIcon( img );
        
        // otočení o 270° pro hover šipku
        
        img_hover = Game.getArrowHover();

        img_hover = rotateImage(img_hover, 270);
        img_hover = img_hover.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_270_hover = new ImageIcon( img_hover );
        
        // --------------------------------------------------
        
        // otočení o 0° pro normální šipku
        
        img = Game.getArrow();

        img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_0 = new ImageIcon( img );
        
        // otočení o 0° pro hover šipku
        
        img_hover = Game.getArrowHover();

        img_hover = rotateImage(img_hover, 0);
        img_hover = img_hover.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_0_hover = new ImageIcon( img_hover );
        
        // --------------------------------------------------
        
        // otočení o 180° pro normální šipku
        
        img = Game.getArrow();

        img = rotateImage(img, 180);
        img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_180 = new ImageIcon( img );
        
        // otočení o 180° pro hover šipku
                
        img_hover = Game.getArrowHover();

        img_hover = rotateImage(img_hover, 180);
        img_hover = img_hover.getScaledInstance( w, h, Image.SCALE_SMOOTH );

        final ImageIcon icon_rot_180_hover = new ImageIcon( img_hover );        

        // vytvoříme na hrací desce šipky + kameny
        for (int i = -1; i < count + 1; i++) {
            for (int j = -1; j < count + 1; j++) {
                
                // vytváříme první řádek šipek
                if (i == -1) {
                    // šipky vytváříme jen na sudých řádcích
                    if ((j + 1) % 2 == 0 && j != -1 && j != count) {

                        JLabel label = new JLabel();
                        label.setOpaque(true);
                        label.setBackground(new Color(0,0,0,0));
                        label.setIcon(icon_rot_90);
                        label.setPreferredSize(new Dimension(w, h));

                        label.putClientProperty("row", i + 1);
                        label.putClientProperty("col", j);

                        this.setMouseListenerForArrows(label, icon_rot_90, icon_rot_90_hover, "col");

                        board_panel.add(label);
                    } 
                    // na lichých řádcích vytvoříme prázdnou oblast
                    else {
                        JPanel b = new JPanel();
                        b.setLayout(new GridBagLayout());
                        b.setOpaque(true);
                        b.setBackground(new Color(0,0,0,0));
                        board_panel.add(b);
                    }

                } 
                // vytváříme poslední řádek šipek
                else if(i == count) {
                    // šipky vytváříme jen na sudých řádcích
                    if ((j + 1) % 2 == 0 && j != -1 && j != count) {

                        JLabel label = new JLabel();
                        label.setOpaque(true);
                        label.setBackground(new Color(0,0,0,0));
                        label.setIcon(icon_rot_270);
                        label.setPreferredSize(new Dimension(w, h));

                        label.putClientProperty("row", i - 1);
                        label.putClientProperty("col", j);

                        this.setMouseListenerForArrows(label, icon_rot_270, icon_rot_270_hover, "col");

                        board_panel.add(label);
                    } 
                    // na lichých řádcích vytvoříme prázdnou oblast
                    else {
                        JPanel b = new JPanel();
                        b.setLayout(new GridBagLayout());
                        b.setOpaque(true);
                        b.setBackground(new Color(0,0,0,0));
                        board_panel.add(b);
                    }
                    
                } 
                // vytváříme první sloupec šipek
                else if(j == -1){
                    // šipky vytváříme jen na sudých sloupcích
                    if ((i + 1) % 2 == 0 && i != -1 && i != count) {

                        JLabel label = new JLabel();
                        label.setOpaque(true);
                        label.setBackground(new Color(0,0,0,0));
                        label.setIcon(icon_rot_0);
                        label.setPreferredSize(new Dimension(w, h));

                        label.putClientProperty("row", i);
                        label.putClientProperty("col", j + 1);

                        this.setMouseListenerForArrows(label, icon_rot_0, icon_rot_0_hover, "row");

                        board_panel.add(label);
                    } 
                    // na lichých sloupcích vytvoříme prázdnou oblast
                    else {
                        JPanel b = new JPanel();
                        b.setLayout(new GridBagLayout());
                        b.setOpaque(true);
                        b.setBackground(new Color(0,0,0,0));
                        board_panel.add(b);
                    }
                    
                } 
                // vytváříme poslední sloupec šipek
                else if(j == count){
                    // šipky vytváříme jen na sudých sloupcích
                    if ((i + 1) % 2 == 0 && i != -1 && i != count) {

                        JLabel label = new JLabel();
                        label.setOpaque(true);
                        label.setBackground(new Color(0,0,0,0));
                        label.setIcon(icon_rot_180);
                        label.setPreferredSize(new Dimension(w, h));

                        label.putClientProperty("row", i);
                        label.putClientProperty("col", j - 1);

                        this.setMouseListenerForArrows(label, icon_rot_180, icon_rot_180_hover, "row");

                        board_panel.add(label);
                    } 
                    // na lichých sloupcích vytvoříme prázdnou oblast
                    else {
                        JPanel b = new JPanel();
                        b.setLayout(new GridBagLayout());
                        b.setOpaque(true);
                        b.setBackground(new Color(0,0,0,0));
                        board_panel.add(b);
                    }

                } 
                // vytváříme kamen
                else {
                    JButton button = new JButton();
                    button.setBorderPainted( false );
                    button.setPreferredSize(new Dimension(w, h));

                    button.putClientProperty("row", i);
                    button.putClientProperty("col", j);

                    button.setOpaque(true);
                    button.setBackground(new Color(0,0,0,0));

                    // vytváříme obsluhu akcí nad kamenem
                    button.addMouseListener(new MouseAdapter() {
                        
                        // při kliknutí na kámen
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            
                            // pokud není zákázaná interakce s hrou a zatím nikdo nevyhrál, zpracujeme kliknutí
                            if (!Game.isWinner() && Game.getCanInteract() == true) {
                                JButton but = (JButton) e.getSource();

                                int act_row = Game.getCurrentPlayer().getRow();
                                int act_col = Game.getCurrentPlayer().getCol();
                                int row = (int) but.getClientProperty("row");
                                int col = (int) but.getClientProperty("col");

                                // pokud vede cesta od aktuální pozice hráče na kamen na nějž hráč kliknul, umístíme hráče na tento kámen pozici
                                if (Game.canGoOn(row, col)) {
                                    boolean did_i_just_take_treasure;
                                    int treasure = Game.getTreasCode(row, col);
                                    boolean can_interact = Game.getCanInteract();
                                    boolean can_shift = Game.getCanShift();
                                    boolean winner = Game.isWinner();
                                    
                                    // zjistíme jestli hráč vzal poklad a případně mu tento poklad započítáme
                                    did_i_just_take_treasure = Game.didIJustTakeTreasure();

                                    // pokud na políčku kam hráč kliknul není poklad který hráč hledá
                                    if (did_i_just_take_treasure == false) {
                                        // uložíme si provedenou akci (přesun hráče bez sebrání pokladu) abychom ji mohli při undo vrátit
                                        Command.capture(Game.getCanInteract(), Game.getLastShiftRow(), Game.getLastShiftCol(), Game.getPlayerDidShift(),
                                                        Game.getIndexCurrenPlayer(), Game.isWinner(), Game.getCanShift(),
                                                        new int[]{Game.getIndexCurrenPlayer(), act_row, act_col});
                                    } 
                                    // pokud na políčku kam hráč kliknul je poklad který hráč hledá
                                    else {
                                        // uložíme si provedenou akci (přesun hráče a sebrání pokladu) abychom ji mohli při undo vrátit
                                        Command.capture(can_interact, Game.getLastShiftRow(), Game.getLastShiftCol(), Game.getPlayerDidShift(),
                                                        Game.getIndexCurrenPlayer(), winner, can_shift,
                                                        new int[]{Game.getIndexCurrenPlayer(), act_row, act_col}, row, col, treasure);
                                    }
                                    
                                    // pokud zatím hráč nevyhrál, nastavíme dalšího hráče na tahu
                                    if (!Game.isWinner()) {
                                        Game.endMove();
                                        if (did_i_just_take_treasure == false) {
                                            GuiJPanel.setColorOfLabelPlayer();
                                            Game.getJPanelInstance().redrawTurnedCards();
                                        }
                                        
                                    } 
                                    // pokud hráč vyhrál, zobrazíme jeho poslední otočenou kartu a zmrazíme hru
                                    else {
                                        GuiJPanel.label_who_play_or_win.setText("Hru vyhrál:");
                                        Game.getJPanelInstance().showCurrentCard();
                                        Game.setCanInteract(false);
                                    }
                                    
                                    // překrelíme minulý a současný kámen hráče (z minulého odebereme hráče a na současný zobrazíme hráče)
                                    redrawStone(act_row, act_col);
                                    redrawStone(row, col);
                                }
                            }
                        }
                        
                        // při stisknutí tlačítka myši nad kamenem, tento kamen zvýrazníme
                        public void mousePressed(MouseEvent e) {
                            JButton but = (JButton) e.getSource();
                            
                            BufferedImage click_dest=toBufferedImage((Image) but.getClientProperty("orig_image"));  

                            RescaleOp opp = new RescaleOp(1.14f, 0, null);
                            click_dest = opp.filter(click_dest, click_dest);
                            
                            repaint();
                            but.setOpaque(false);
                            but.setIcon(new ImageIcon( (Image) click_dest ));
                            but.setOpaque(true);
                            but.setBackground(new Color(0,0,0,0));
                        }

                        // při uvolěnní tlačítka myši nad kamenem, tento kamen odzvýrazníme
                        public void mouseReleased(MouseEvent e) {
                            JButton but = (JButton) e.getSource();
                            //btn.setText("r:" + btn.getClientProperty("row") + " c:" + btn.getClientProperty("col"));
                            
                            repaint();
                            but.setOpaque(false);
                            but.setIcon(new ImageIcon( (Image) but.getClientProperty("orig_image") ));
                            but.setOpaque(true);
                            but.setBackground(new Color(0,0,0,0));
                        }
                        
                        // při přejetí myší nad kamenem, tento kamen zvýrazníme
                        public void mouseEntered(MouseEvent e) { 
                                    
                            JButton but = (JButton) e.getSource();
                            repaint();
                            but.setOpaque(false);
                            but.setIcon(new ImageIcon( (Image) but.getClientProperty("hover_image") ));
                            but.setOpaque(true);
                            but.setBackground(new Color(0,0,0,0));
                                 
                        }
 
                        // při odjetí myší z kamene, tento kamen odzvýrazníme
                        public void mouseExited(MouseEvent e) { 
                             
                            JButton but = (JButton) e.getSource();
                            repaint();
                            but.setOpaque(false);
                            but.setIcon(new ImageIcon( (Image) but.getClientProperty("orig_image") ));
                            but.setOpaque(true);
                            but.setBackground(new Color(0,0,0,0));
                        }
                    });
                    
                    // přidáme kámen do kolekce kamenů, které později vykreslíme
                    Game.getButtonStones().add(button);

                    board_panel.add(button);
                }
            }
        }
        
        // board panel přidáme do hlavního panelu hry
        Game.getJPanelInstance().add(board_panel);    
        
        // vytvoříme pravý panel - hned vedle panelu pro zobrazení desky
        JPanel right_panel = new JPanel();
        right_panel.setLayout(new BoxLayout(right_panel, BoxLayout.PAGE_AXIS));
        
        right_panel.setPreferredSize(new Dimension((count - count / 3) * (w), (count + 2) * (h + gap)));
        GuiJPanel.right_panel_width = (count - count / 3) * (w);
        right_panel.setOpaque(true);
        right_panel.setBackground(new Color(0,0,0,0));

        // přidáme mezeru
        
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 25,Game.getWindowHeight() / 25)));
        
        // vykreslíme údaje o tom který hráč hraje - reprezentováno pomocí JLabelů
        JPanel panel_who_play = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel_who_play.setOpaque(true);
        panel_who_play.setBackground(new Color(0,0,0,0));
        
        GuiJPanel.label_who_play_or_win = new JLabel();
        GuiJPanel.setLabelWhoPlayOrWin();
        GuiJPanel.label_who_play_or_win.setForeground(new Color(95,102,88,240));

        GuiJPanel.label_who_play_name = new JLabel("");
        GuiJPanel.setColorOfLabelPlayer();
        
        GuiJPanel.label_who_play_or_win.setFont(new Font("New Courier", Font.BOLD, Game.getWindowHeight() / 35));
        
        GuiJPanel.label_who_play_name.setFont(new Font("New Courier", Font.BOLD, Game.getWindowHeight() / 35));
        
        panel_who_play.add(GuiJPanel.label_who_play_or_win);
        panel_who_play.add(GuiJPanel.label_who_play_name);
        
        right_panel.add(panel_who_play);
        
        // přidáme text "volný kámen" - reprezentováno pomocí JLabelu
        JLabel label = new JLabel("Volný kámen");
        label.setFont(new Font("Georgia", Font.BOLD, Game.getWindowHeight() / 35));
        label.setForeground(new Color(140,148,132,205));
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        right_panel.add(label);
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 35,Game.getWindowHeight() / 35)));
    
        // přidáme VOLNÝ KÁMEN - reprezentováno pomocí JLabelu
        JLabel free_stone_label = new JLabel();
        free_stone_label.setMaximumSize(new Dimension(Game.getWindowHeight() / 8, Game.getWindowHeight() / 8));
        free_stone_label.setAlignmentX(Component.CENTER_ALIGNMENT);
        free_stone_label.setOpaque(true);
        free_stone_label.setBackground(new Color(0,0,0,0));

        // přidáme obsluhu událostí při kliknutí na volný kámen
        free_stone_label.addMouseListener(new MouseAdapter() {
            // při kliknutí na kámen otočíme kámen 
            @Override
            public void mouseClicked(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource(); 

                Game.getBoard().getFreeStone().turnRight();

                Image imgg = Game.getStones(Game.getStoneMap().get(Game.getTypeOfFreeStone()));
            
                imgg = rotateImage(imgg, Game.getRotationOfFreeStone());
                            
                Image imgg_treasure = Game.getTreasures(Game.getFreeTreasCode());

                imgg = mergeImage(imgg, imgg_treasure);

                imgg = imgg.getScaledInstance(Game.getWindowHeight() / 8, Game.getWindowHeight() / 8, Image.SCALE_SMOOTH ) ;
                lab.putClientProperty("orig_image", imgg);
                                
                BufferedImage destt = toBufferedImage(imgg);  

                RescaleOp opp = new RescaleOp(1.07f, 0, null);
                destt = opp.filter(destt, destt);
                                
                lab.putClientProperty("hover_image", (Image) destt);
                            
                repaint();
                lab.setOpaque(false);
                lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("hover_image") ));
                lab.setOpaque(true);
                lab.setBackground(new Color(0,0,0,0));
                            
            }
            
            // při stisknutí tlačítka myši nad kamenem, tento kamen zvýrazníme            
            public void mousePressed(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource();
                            
                BufferedImage click_dest=toBufferedImage((Image) lab.getClientProperty("orig_image"));  

                RescaleOp opp = new RescaleOp(1.14f, 0, null);
                click_dest = opp.filter(click_dest, click_dest);
                            
                repaint();
                lab.setOpaque(false);
                lab.setIcon(new ImageIcon( (Image) click_dest ));
                lab.setOpaque(true);
                lab.setBackground(new Color(0,0,0,0));
            }

            // při uvolnění tlačítka myši nad kamenem, tento kamen odzvýrazníme
            public void mouseReleased(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource();

                repaint();
                lab.setOpaque(false);
                lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("orig_image") ));
                lab.setOpaque(true);
                lab.setBackground(new Color(0,0,0,0));
            }
                        
            // při přejetí myší nad kamenem, tento kamen zvýrazníme                 
            public void mouseEntered(MouseEvent e) { 
                JLabel lab = (JLabel) e.getSource();
                                
                repaint();
                lab.setOpaque(false);
                lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("hover_image") ));
                lab.setOpaque(true);
                lab.setBackground(new Color(0,0,0,0));
                                 
            }
    
            // při odjetí myší z kamene, tento kamen odzvýrazníme
            public void mouseExited(MouseEvent e) { 
                JLabel lab = (JLabel) e.getSource();
                                
                repaint();
                lab.setOpaque(false);
                lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("orig_image") ));
                lab.setOpaque(true);
                lab.setBackground(new Color(0,0,0,0));
            }
        });
        
        // přidáme volný kámen do třídy Game kdy jej později vykreslíme
        Game.setFreeStoneLabel(free_stone_label);
        
        right_panel.add(free_stone_label);
        
        // přidáme mezeru
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 25,Game.getWindowHeight() / 25)));
        
        // přidáme text "karta hráče na tahu"
        JLabel label_player_card = new JLabel("Karta hráče na tahu");
        label_player_card.setFont(new Font("Georgia", Font.BOLD, Game.getWindowHeight() / 35));
        label_player_card.setForeground(new Color(140,148,132,205));
        label_player_card.setAlignmentX(Component.CENTER_ALIGNMENT);
        right_panel.add(label_player_card);
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 35,Game.getWindowHeight() / 35)));
        
        // přidáme aktuální otočenou kartu hráče
        GuiJPanel.actual_card = new JLabel();
        GuiJPanel.actual_card.setMaximumSize(new Dimension((int) ((Game.getWindowHeight() / 20) * 3.56), (int) ((Game.getWindowHeight() / 20) * 5.53)));
        GuiJPanel.actual_card.setAlignmentX(Component.CENTER_ALIGNMENT);
        GuiJPanel.actual_card.setOpaque(true);
        GuiJPanel.actual_card.setBackground(new Color(0,0,0,0));

        // přidáme obsluhu pro události na kartě
        actual_card.addMouseListener(new MouseAdapter() {
                    
            // pokud je stisknuto tlačítko myši nad kartou, zobrazíme ji
            public void mousePressed(MouseEvent e) {
                // pokud je dovolena interakce s hrou a hráč nevyhrál, zobrazíme kartu
                if (!Game.isWinner() && Game.getCanInteract() == true) {
                    Image imgg = Game.getTreasureCardImageForCurrentPlayer();
                
                    BufferedImage destt=toBufferedImage(imgg);  
                    imgg = destt.getScaledInstance((int) ((Game.getWindowHeight() / 20) * 3.56), (int) ((Game.getWindowHeight() / 20) * 5.53),Image.SCALE_SMOOTH ) ;
                                
                    JLabel lab = (JLabel) e.getSource();
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(new ImageIcon(imgg));
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }

            // pokud je puštěno tlačítko myši nad kartou, skryjeme ji
            public void mouseReleased(MouseEvent e) {
                // pokud je dovolena interakce s hrou a hráč nevyhrál, skyryjeme kartu
                if (!Game.isWinner() && Game.getCanInteract() == true) {
                    JLabel lab = (JLabel) e.getSource();
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("hover_image") ));
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }
                        
            // pokud hráč přejel myší myši nad kartou, zvýrazníme ji             
            public void mouseEntered(MouseEvent e) { 
                if (!Game.isWinner() && Game.getCanInteract() == true) {    
                    JLabel lab = (JLabel) e.getSource();
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("hover_image") ));
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
                                 
            }
    
            // pokud hráč odjel myší myši z karty, odzvýrazníme ji   
            public void mouseExited(MouseEvent e) { 
                if (!Game.isWinner() && Game.getCanInteract() == true) {
                    JLabel lab = (JLabel) e.getSource();
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("orig_image") ));
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }
        });
        
        // uložíme si do třídy Game JLabel s kartou
        Game.setLabelActualCard(actual_card);
        
        right_panel.add(actual_card);
        
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 25,Game.getWindowHeight() / 25)));
        
        // zobrazíme text "Otočené karty hráče na tahu"
        JLabel label_player_turned_cards = new JLabel("Otočené karty hráče na tahu");
        label_player_turned_cards.setFont(new Font("Georgia", Font.BOLD, Game.getWindowHeight() / 35));
        label_player_turned_cards.setForeground(new Color(140,148,132,205));
        label_player_turned_cards.setAlignmentX(Component.CENTER_ALIGNMENT);
        right_panel.add(label_player_turned_cards);
        right_panel.add(Box.createRigidArea(new Dimension(Game.getWindowHeight() / 35,Game.getWindowHeight() / 35)));
    
        
        // vytvoříme otočené karty hráče na tahu
        JPanel turned_cards_panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 2, 0));
        ((FlowLayout)turned_cards_panel.getLayout()).setVgap(gap);
        turned_cards_panel.setOpaque(true);
        turned_cards_panel.setBackground(new Color(0,0,0,0));
        
        GuiJPanel.turned_card_width = (int) ((GuiJPanel.right_panel_width * 3.56) / (11.0 + (double) (3 * Game.getNumberOfTreasures() / Game.getNumberOfPlayers())));
        GuiJPanel.turned_card_height = (int) ((GuiJPanel.right_panel_width * 5.53) / (11.0 + (double) (3 * Game.getNumberOfTreasures() / Game.getNumberOfPlayers())));
        
        for (int i = 0; i < Game.getNumberOfTreasures() / Game.getNumberOfPlayers(); i++) {
            JLabel turned_card = new JLabel();
            turned_card.setPreferredSize(new Dimension(GuiJPanel.turned_card_width, GuiJPanel.turned_card_height));

            turned_cards_panel.add(turned_card);
        
            Game.getLabelTurnedCards().add(turned_card);
        }
        right_panel.add(turned_cards_panel);
        
        // roztahovadlo, které pomůže v layoutu
        JPanel fun = new JPanel(new FlowLayout(FlowLayout.CENTER));
        fun.setOpaque(true);
        fun.setBackground(new Color(0,0,0,0));
        right_panel.add(fun);
        
        Game.getJPanelInstance().add(right_panel);  
        Game.getJFramelInstance().pack();
    }
    /**
     * Metoda pro překreslení otočených karet aktuálního hráče
     */
    public void redrawTurnedCards() {
        JLabel label;
        Image img;
        BufferedImage dest;
        for (int i = 0; i <  Game.getLabelTurnedCards().size(); i++) {
            label = Game.getLabelTurnedCards().get(i);   
            img = Game.getCurrentPlayer().getTurnedCard(i); 
            
            dest=this.toBufferedImage(img);  
        
            img = dest.getScaledInstance(GuiJPanel.turned_card_width, GuiJPanel.turned_card_height,Image.SCALE_SMOOTH ) ;
            ImageIcon icon = new ImageIcon( img );
            
            label.setOpaque(false);
            label.setIcon(icon);
            label.setOpaque(true);
            label.setBackground(new Color(0,0,0,0));
        }
    }
    
    /**
     * Metoda pro vykreslení základních komponent hry
     */
    public void redrawBoard() {

        int w = Game.getWidthStone();
        int h = Game.getHeightStone();

        JButton button;
        String type_of_stone;
        int rotation, row, col, treasure;

        // vykreslení hrací desky (kamenů na hrací desce)
        for (int i = 0; i < Game.getSizeOfBoard() * Game.getSizeOfBoard(); i++) {
            button = Game.getButtonStones().get(i);
            
            row = i / Game.getSizeOfBoard();
            col = i % Game.getSizeOfBoard();

            type_of_stone = Game.getType(row, col);
            Image img = Game.getStones(Game.getStoneMap().get(type_of_stone));
            
            rotation = Game.getRotation(row, col);
            img = rotateImage(img, rotation);

            treasure = Game.getTreasCode(row, col);

            // pokud je kamenu poklad, sloučíme jej s kamenem do jednoho obrázku
            if (treasure != 0) {
                Image img_treasure = Game.getTreasures(treasure);
                img = mergeImage(img, img_treasure);
            }

            // pokud je kamenu jeden a více hráčů, sloučíme je s kamenem a popř. pokladem do jednoho obrázku
            for (int j = 0; j < Game.getNumberOfPlayers(); j++) {
                if (Game.getPlayer(j).getRow() == row && Game.getPlayer(j).getCol() == col) {
                    img = mergeImage(img, Game.getFigures(j + 1));
                }
            }

            img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH ) ;
            button.putClientProperty("orig_image", img);
            
            BufferedImage dest=this.toBufferedImage(img);

            RescaleOp op = new RescaleOp(1.07f, 0, null);
            dest = op.filter(dest, dest);
            
            button.putClientProperty("hover_image", (Image) dest);
            
            ImageIcon icon = new ImageIcon( img );

            button.setOpaque(false);
            button.setIcon(icon);
            button.setOpaque(true);
            button.setBackground(new Color(0,0,0,0));
        }
        
        // vykreslíme volný kámen
        
        Image img = Game.getStones(Game.getStoneMap().get(Game.getTypeOfFreeStone()));
            
        img = rotateImage(img, Game.getRotationOfFreeStone());

        Image img_treasure = Game.getTreasures(Game.getFreeTreasCode());
        
        // sloučíme kamen s pokladem který na něm je
        img = mergeImage(img, img_treasure);

        img = img.getScaledInstance( Game.getWindowHeight() / 8, Game.getWindowHeight() / 8, Image.SCALE_SMOOTH ) ;
        Game.getFreeStoneLabel().putClientProperty("orig_image", img);
            
        BufferedImage dest=this.toBufferedImage(img);  

        RescaleOp op = new RescaleOp(1.07f, 0, null);
        dest = op.filter(dest, dest);
            
        Game.getFreeStoneLabel().putClientProperty("hover_image", (Image) dest);
            
        ImageIcon icon = new ImageIcon( img );
        
        Game.getFreeStoneLabel().setOpaque(false);
        Game.getFreeStoneLabel().setIcon(icon);
        Game.getFreeStoneLabel().setOpaque(true);
        Game.getFreeStoneLabel().setBackground(new Color(0,0,0,0));
        
        // vykreslíme kartu hráče
        
        // pokud není hárč vítěz, vykreslíme pozadí karty
        if (!Game.isWinner()) {
            img = Game.getCardBackside();
        }
        // pokud je háč vítěz, vykreslíme kartu
        else {
            img = Game.getTreasureCardImageForCurrentPlayer();
        }
            
        dest=this.toBufferedImage(img);  
        
        img = dest.getScaledInstance((int) ((Game.getWindowHeight() / 20) * 3.56), (int) ((Game.getWindowHeight() / 20) * 5.53),Image.SCALE_SMOOTH ) ;
        Game.getLabelActualCard().putClientProperty("orig_image", img);
        icon = new ImageIcon( img );
        
        op = new RescaleOp(1.07f, 0, null);
        dest = op.filter(dest, dest);
        img = dest.getScaledInstance((int) ((Game.getWindowHeight() / 20) * 3.56), (int) ((Game.getWindowHeight() / 20) * 5.53),Image.SCALE_SMOOTH ) ;

            
        Game.getLabelActualCard().putClientProperty("hover_image", img);
        
        Game.getLabelActualCard().setOpaque(false);
        Game.getLabelActualCard().setIcon(icon);
        Game.getLabelActualCard().setOpaque(true);
        Game.getLabelActualCard().setBackground(new Color(0,0,0,0));
    }


    /**
     * Metoda prekresli radek row a volny kamen
     * @param row radke ktery se bude prekreslovat
     */
    public void redrawRow (int row) {
        int w = Game.getWidthStone();
        int h = Game.getHeightStone();

        JButton button;
        String type_of_stone;

        int rotation, treasure;

        // překresujeme jen příslušný řádek na hrací desce
        for (int col = 0; col < Game.getSizeOfBoard(); col++) {
            button = Game.getButtonStones().get(row * Game.getSizeOfBoard() + col);

            type_of_stone = Game.getType(row, col);
            Image img = Game.getStones(Game.getStoneMap().get(type_of_stone));
            
            rotation = Game.getRotation(row, col);
            img = rotateImage(img, rotation);

            treasure = Game.getTreasCode(row, col);

            // pokud je kamenu poklad, sloučíme jej s kamenem do jednoho obrázku
            if (treasure != 0) {
                Image img_treasure = Game.getTreasures(treasure);
                img = mergeImage(img, img_treasure);
            }
            // pokud je kamenu jeden a více hráčů, sloučíme je s kamenem a popř. pokladem do jednoho obrázku
            for (int j = 0; j < Game.getNumberOfPlayers(); j++) {
                if (Game.getPlayer(j).getRow() == row && Game.getPlayer(j).getCol() == col) {
                    img = mergeImage(img, Game.getFigures(j + 1));
                }
            }

            img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH ) ;
            button.putClientProperty("orig_image", img);
            
            BufferedImage dest=this.toBufferedImage(img);

            RescaleOp op = new RescaleOp(1.07f, 0, null);
            dest = op.filter(dest, dest);
            
            button.putClientProperty("hover_image", (Image) dest);
            
            ImageIcon icon = new ImageIcon( img );

            button.setOpaque(false);
            button.setIcon(icon);
            button.setOpaque(true);
            button.setBackground(new Color(0,0,0,0));
        }

        // překreslíme volný kámen
        Image img = Game.getStones(Game.getStoneMap().get(Game.getTypeOfFreeStone()));
            
        img = rotateImage(img, Game.getRotationOfFreeStone());

        Image img_treasure = Game.getTreasures(Game.getFreeTreasCode());

        // sloučíme kamen s pokladem který na něm je
        img = mergeImage(img, img_treasure);

        img = img.getScaledInstance( Game.getWindowHeight() / 8, Game.getWindowHeight() / 8, Image.SCALE_SMOOTH ) ;
        Game.getFreeStoneLabel().putClientProperty("orig_image", img);
            
        BufferedImage dest=this.toBufferedImage(img);  

        RescaleOp op = new RescaleOp(1.07f, 0, null);
        dest = op.filter(dest, dest);
            
        Game.getFreeStoneLabel().putClientProperty("hover_image", (Image) dest);
            
        ImageIcon icon = new ImageIcon( img );
        
        Game.getFreeStoneLabel().setOpaque(false);
        Game.getFreeStoneLabel().setIcon(icon);
        Game.getFreeStoneLabel().setOpaque(true);
        Game.getFreeStoneLabel().setBackground(new Color(0,0,0,0));
    }

    /**
     * Metoda Prekresli sloupec col a volny kamen
     * @param col sloupec ktery se bude prekreslovat
     */
    public void redrawCol (int col) {
        int w = Game.getWidthStone();
        int h = Game.getHeightStone();

        JButton button;
        String type_of_stone;

        int rotation, treasure;
        // překresujeme jen příslušný sloupec na hrací desce
        for (int row = 0; row < Game.getSizeOfBoard(); row++) {
            button = Game.getButtonStones().get(row * Game.getSizeOfBoard() + col);

            type_of_stone = Game.getType(row, col);
            Image img = Game.getStones(Game.getStoneMap().get(type_of_stone));
            
            rotation = Game.getRotation(row, col);
            img = rotateImage(img, rotation);

            treasure = Game.getTreasCode(row, col);
            
            // pokud je kamenu poklad, sloučíme jej s kamenem do jednoho obrázku
            if (treasure != 0) {
                Image img_treasure = Game.getTreasures(treasure);
                img = mergeImage(img, img_treasure);
            }
            // pokud je kamenu jeden a více hráčů, sloučíme je s kamenem a popř. pokladem do jednoho obrázku
            for (int j = 0; j < Game.getNumberOfPlayers(); j++) {
                if (Game.getPlayer(j).getRow() == row && Game.getPlayer(j).getCol() == col) {
                    img = mergeImage(img, Game.getFigures(j + 1));
                }
            }

            img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH ) ;
            button.putClientProperty("orig_image", img);
            
            BufferedImage dest=this.toBufferedImage(img);

            RescaleOp op = new RescaleOp(1.07f, 0, null);
            dest = op.filter(dest, dest);
            
            button.putClientProperty("hover_image", (Image) dest);
            
            ImageIcon icon = new ImageIcon( img );

            button.setOpaque(false);
            button.setIcon(icon);
            button.setOpaque(true);
            button.setBackground(new Color(0,0,0,0));
        }

        // překreslíme volný kámen
        Image img = Game.getStones(Game.getStoneMap().get(Game.getTypeOfFreeStone()));
            
        img = rotateImage(img, Game.getRotationOfFreeStone());

        Image img_treasure = Game.getTreasures(Game.getFreeTreasCode());

        // sloučíme kamen s pokladem který na něm je
        img = mergeImage(img, img_treasure);

        img = img.getScaledInstance( Game.getWindowHeight() / 8, Game.getWindowHeight() / 8, Image.SCALE_SMOOTH ) ;
        Game.getFreeStoneLabel().putClientProperty("orig_image", img);
            
        BufferedImage dest=this.toBufferedImage(img);  

        RescaleOp op = new RescaleOp(1.07f, 0, null);
        dest = op.filter(dest, dest);
            
        Game.getFreeStoneLabel().putClientProperty("hover_image", (Image) dest);
            
        ImageIcon icon = new ImageIcon( img );
        
        Game.getFreeStoneLabel().setOpaque(false);
        Game.getFreeStoneLabel().setIcon(icon);
        Game.getFreeStoneLabel().setOpaque(true);
        Game.getFreeStoneLabel().setBackground(new Color(0,0,0,0));
    }

    /**
     * Metoda prekresli kamen na souřadnicích row a col
     * @param row souřadnice řádku kamene který se bude překreslovat
     * @param col souřadnice sloupce kamene který se bude překreslovat
     */
    public void redrawStone (int row, int col) {
        int w = Game.getWidthStone();
        int h = Game.getHeightStone();

        JButton button;
        String type_of_stone;

        int rotation, treasure;

        button = Game.getButtonStones().get(row * Game.getSizeOfBoard() + col);

        type_of_stone = Game.getType(row, col);
        Image img = Game.getStones(Game.getStoneMap().get(type_of_stone));
            
        rotation = Game.getRotation(row, col);
        img = rotateImage(img, rotation);

        treasure = Game.getTreasCode(row, col);
        
        // pokud je kamenu poklad, sloučíme jej s kamenem do jednoho obrázku
        if (treasure != 0) {
            Image img_treasure = Game.getTreasures(treasure);
            img = mergeImage(img, img_treasure);
        }
            
        // pokud je kamenu jeden a více hráčů, sloučíme je s kamenem a popř. pokladem do jednoho obrázku    
        for (int j = 0; j < Game.getNumberOfPlayers(); j++) {
            if (Game.getPlayer(j).getRow() == row && Game.getPlayer(j).getCol() == col) {
                img = mergeImage(img, Game.getFigures(j + 1));
            }
        }

        img = img.getScaledInstance( w, h, Image.SCALE_SMOOTH ) ;
        button.putClientProperty("orig_image", img);
            
        BufferedImage dest=this.toBufferedImage(img);

        RescaleOp op = new RescaleOp(1.07f, 0, null);
        dest = op.filter(dest, dest);
            
        button.putClientProperty("hover_image", (Image) dest);
            
        ImageIcon icon = new ImageIcon( img );

        button.setOpaque(false);
        button.setIcon(icon);
        button.setOpaque(true);
        button.setBackground(new Color(0,0,0,0));
    }
    
    /**
     * Metoda překreslíaktuální katu hráče na pozadí této karty
     */
    public void redrawActualCard() {
        JLabel lab = Game.getLabelActualCard();
        repaint();
        lab.setOpaque(false);
        lab.setIcon(new ImageIcon( (Image) lab.getClientProperty("orig_image") ));
        lab.setOpaque(true);
        lab.setBackground(new Color(0,0,0,0));    
    }

    /**
     * Metoda Prevede Image img na BufferedImage
     * @param img obrázek Image který se bude převádět do BufferedImage
     * @return převedený obrázek jako typ BufferedImage
     */
    public static BufferedImage toBufferedImage(Image img){
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        return bimage;
    }

    /**
     * Metoda Otoci obrazek img o pozadovany stupen degrees
     * @param img obrázek který se bude otáčet
     * @param degrees stupně o kolik se bude obrázek otáčet
     * @return otočený obrázek
     */
    public static Image rotateImage(Image img, int degrees) {
        BufferedImage b_img = toBufferedImage(img);

        double rot_angle = Math.toRadians(degrees);
        double rot_point_x = img.getWidth(null) / 2;
        double rot_point_y = img.getHeight(null) / 2;

        AffineTransform aff_trans = AffineTransform.getRotateInstance(rot_angle, rot_point_x, rot_point_y);
        AffineTransformOp aff_trans_op = new AffineTransformOp(aff_trans, AffineTransformOp.TYPE_BILINEAR);

        return aff_trans_op.filter(b_img, null);
    }

    /**
     * Metoda smicha dva zadane obrazky do jednoho
     * @param img1 obrázek první
     * @param img2 obrázek druhý
     * @return výsledný obrázek vzniklý sloučením
     */
    public static Image mergeImage(Image img1, Image img2) {
        BufferedImage b_img1 = toBufferedImage(img1);
        BufferedImage b_img2 = toBufferedImage(img2);
        Graphics2D g2D = b_img1.createGraphics();

        g2D.drawImage(b_img1, 0, 0, null);
        g2D.drawImage(b_img2, 0, 0, null);  
        g2D.dispose();

        return b_img1;
    }
    
    /**
     * Metoda otočí kartu aktuálního hráče
     */
    public void showCurrentCard() {
        
        Image imgg = Game.getTreasureCardImageForCurrentPlayer();
            
        BufferedImage destt=toBufferedImage(imgg);  
        imgg = destt.getScaledInstance((int) ((Game.getWindowHeight() / 20) * 3.56), (int) ((Game.getWindowHeight() / 20) * 5.53),Image.SCALE_SMOOTH ) ;

        repaint();
        GuiJPanel.actual_card.setOpaque(false);
        GuiJPanel.actual_card.setIcon(new ImageIcon(imgg));
        GuiJPanel.actual_card.setOpaque(true);
        GuiJPanel.actual_card.setBackground(new Color(0,0,0,0));    
    }
    
    /**
     * Metoda schová kartu aktuálního hráče
     */
    public void hideCurrentCard() {
        
        repaint();
        GuiJPanel.actual_card.setOpaque(false);
        GuiJPanel.actual_card.setIcon(new ImageIcon( (Image) GuiJPanel.actual_card.getClientProperty("hover_image") ));
        GuiJPanel.actual_card.setOpaque(true);
        GuiJPanel.actual_card.setBackground(new Color(0,0,0,0)); 
    }
    
    /**
     * Metoda nastaví číslo hráče a jeho barvu který je na tahu 
     */
    public static void setColorOfLabelPlayer() {
        GuiJPanel.label_who_play_name.setText("hráč "+ Integer.toString(Game.getOrder()));
        
        if (Game.getOrder() == 1) {
            GuiJPanel.label_who_play_name.setForeground(new Color(255,0,0,255));
        }
        else if (Game.getOrder() == 2) {
            GuiJPanel.label_who_play_name.setForeground(new Color(0,222,0,255));
        }
        else if (Game.getOrder() == 3) {
            GuiJPanel.label_who_play_name.setForeground(new Color(0,22,222,255));
        }
        else if (Game.getOrder() == 4) {
            GuiJPanel.label_who_play_name.setForeground(new Color(230,210,0,255));
        }
    }
    
    /**
     * Metoda nastaví ošetření akcí nad šipkami pro posun kamenů
     * @param label label
     * @param icon_rot icon_rot
     * @param icon_rot_hover icon_rot_hover
     * @param redraw_col_or_row redraw_col_or_row
     */
    public void setMouseListenerForArrows(JLabel label, final ImageIcon icon_rot, final ImageIcon icon_rot_hover, final String redraw_col_or_row) {
        label.addMouseListener(new MouseAdapter() {
                          
            // při kliknutí na šipku
            @Override
            public void mouseClicked(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource();
                // pokud můžeme provést daný posun kamene
                if (Game.canShift( (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")) && Game.getCanInteract() == true) {
                    Game.getBoard().shift(Game.getBoard().get((int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")));

                    // posuneme hráče na daném sloupci nebo řádku
                    int[][] players = new int[Game.getNumberOfPlayers()][3];
                    for (int i = 0; i < Game.getNumberOfPlayers(); i++) {
                        players[i][0] = i;
                        players[i][1] = Game.getPlayer(i).getRow();
                        players[i][2] = Game.getPlayer(i).getCol();
                    }

                    // zachytíme posun jako akci, kterou později undem vrátíme 
                    Command.capture(Game.getCanInteract(), Game.getLastShiftRow(), Game.getLastShiftCol(), Game.getPlayerDidShift(),
                                        Game.getIndexCurrenPlayer(), Game.isWinner(), Game.getCanShift(),
                                        (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col"),
                                        Game.getRotation((int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")),
                                        players);

                    // provedeme posun
                    Game.playerShift((int) lab.getClientProperty("row"), (int) lab.getClientProperty("col"));

                    // překreslíme příslušný sloupec nebo řádek
                    if (redraw_col_or_row == "col") {
                        redrawCol((int) lab.getClientProperty("col"));    
                    }
                    else {
                        redrawRow((int) lab.getClientProperty("row"));    
                    }
                                    
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(icon_rot);
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }
            
            // pokud hráč stiskl tlačátko myši nad šipkou
            public void mousePressed(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource();
                // pokud je možné posunovat kamen tak se šipka zvýrazní
                if (Game.canShift( (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")) && Game.getCanInteract() == true) {
                    lab.setOpaque(false);
                    lab.setBackground(Color.BLACK);
                }
            }
            
            // pokud hráč pustil tlačítko myši nad šipkou
            public void mouseReleased(MouseEvent e) {
                JLabel lab = (JLabel) e.getSource();
                // pokud je možné posunovat kamen tak se šipka odzvýrazní
                if (Game.canShift( (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")) && Game.getCanInteract() == true) {
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(icon_rot_hover);
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }
             
            // pokud hráč přejel myší nad šipkou
            public void mouseEntered(MouseEvent e) { 
                JLabel lab = (JLabel) e.getSource();
                // pokud je možné posunovat kamen tak se šipka zvýrazní
                if (Game.canShift( (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")) && Game.getCanInteract() == true) {
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(icon_rot_hover);
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
                                 
            }
 
            // pokud hráč opustil myší šipku
            public void mouseExited(MouseEvent e) { 
                JLabel lab = (JLabel) e.getSource();
                // pokud je možné posunovat kamen tak se šipka odzvýrazní
                if (Game.canShift( (int) lab.getClientProperty("row"), (int) lab.getClientProperty("col")) && Game.getCanInteract() == true) {
                    repaint();
                    lab.setOpaque(false);
                    lab.setIcon(icon_rot);
                    lab.setOpaque(true);
                    lab.setBackground(new Color(0,0,0,0));
                }
            }
        });
    }
    
    /**
     * Metoda nastaví text na tahu je nebo hru vyhrál podle stavu hry
     */
    public static void setLabelWhoPlayOrWin() {
        if (!Game.isWinner()) {
            GuiJPanel.label_who_play_or_win.setText("Na tahu je:");
                                        
        } else {
            GuiJPanel.label_who_play_or_win.setText("Hru vyhrál:");
        }   
    }

    /**
     * Metoda vykreslí komponentu
     */
    public void paintComponent(Graphics g) {
        //vykreslení pozadí
        super.paintComponent(g);
        
        // vykreslíme texturu
        Image img = new ImageIcon(getClass().getResource("/graphics/textura1.png")).getImage();
        Graphics2D g2 = (Graphics2D) g;
        
        BufferedImage  mImage = toBufferedImage(img);
        Rectangle2D tr = new Rectangle2D.Double(0, 0, mImage.getWidth(), mImage.getHeight());
        TexturePaint tp = new TexturePaint(mImage, tr);
        g2.setPaint(tp);
        Rectangle2D  r = (Rectangle2D) this.getBounds();  
        g2.fill(r);
    }
}