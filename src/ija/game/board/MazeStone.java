/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu MazeStone, viz dokumentační komentář této třídy 
 */

package ija.game.board;

import ija.game.treasure.Treasure;

/**
 * Třída reprezentuj jeden kámen, který se umisťuje na políčka hrací plochy.
 * Kámen uchovává informaci o cestě - směrech kudy lze kámen opustit
 * @author Jan Herec, Pavel Juhaňák
 */
public class MazeStone {
    private String type;
    private boolean[] direction = new boolean[4];
    private int rotation;

    private Treasure treasure;

    public static enum CANGO { LEFT, UP, RIGHT, DOWN; }

    /**
     * Vytvoří kámen podle zadaného typu type. 
     * @param type typ nově vytvořeného kamenu. Typ může být:
     *             "C" - vytvoří kámen reprezentující rohovou cestu (ve tvaru písmena L).
     *                   Po inicializaci směřuje cesta zleva nahoru
     *             "L" - vytvoří kámen reprezentující rovnou cestu.
     *                   po inicializaci směřuje cesta zleva doprava
     *             "F" - vytvoří kámen reprezentující rozdělení cesty (ve tvaru písman T).
     *                   po inicializaci směřuje cesta zleva nahoru a doprava
     * @throws IllegalArgumentException pokud nelze kámen vytvořit (chybný řetězec type)
     */
    private MazeStone (String type) {
        if (!type.equals("C") && !type.equals("L") && !type.equals("F")) {
            String msg = "Unexpected argument [" + type + "]";
            throw new IllegalArgumentException(msg);
        }


        if (type.equals("C")) {
            this.direction[CANGO.LEFT.ordinal()]  = true;
            this.direction[CANGO.UP.ordinal()]    = true;
            this.direction[CANGO.RIGHT.ordinal()] = false;
            this.direction[CANGO.DOWN.ordinal()]  = false;
            
            this.type = "C";
        }
        else if (type.equals("L")) {
            this.direction[CANGO.LEFT.ordinal()]  = true;
            this.direction[CANGO.UP.ordinal()]    = false;
            this.direction[CANGO.RIGHT.ordinal()] = true;
            this.direction[CANGO.DOWN.ordinal()]  = false;
            
            this.type = "L";
        }
        else if (type.equals("F")) {
            this.direction[CANGO.LEFT.ordinal()]  = true;
            this.direction[CANGO.UP.ordinal()]    = true;
            this.direction[CANGO.RIGHT.ordinal()] = true;
            this.direction[CANGO.DOWN.ordinal()]  = false;     
            
            this.type = "F";
        }

        this.rotation = 0;

    }

    /**
     * Vytvoří kámen podle zadaného typu type, tím, že volá konstruktor.
     * @param type typ nově vytvořeného kamenu.
     * @return vrací nově vytvořený kámen
     */
    public static MazeStone create(String type) {
        return new MazeStone(type);
    }

    /**
     * Vrací informaci, zdali je možné kámen opustit daným směrem
     * @param dir směr, pro který zjišťujeme, jestli je pomocí něj možné kámen opustit 
     * @return vrací true - pokud lze daným směrem kámen opustit, jinak vrací false
     */
    public boolean canGo (MazeStone.CANGO dir) {
        return this.direction[dir.ordinal()];
    }
    
    /**
     * Vrací atribut type
     * @return this.type
     */
    public String getType() {
        return this.type;
    }

    /**
     * Vrací atribut rotation
     * @return this.rotation
     */
    public int getRotation() {
        return this.rotation;
    }
    
    /**
     * Nastaví atribut rotation
     * @param rot požadovaná rotace
     */
    public void setRotation(int rot) {
        this.rotation = rot;
    }

    /**
     * Otočí kámen o 90° doprava
     */
    public void turnRight () {
        boolean tmp = this.direction[CANGO.LEFT.ordinal()];

        this.rotation = (this.rotation + 90) % 360;

        this.direction[CANGO.LEFT.ordinal()]  = this.direction[CANGO.DOWN.ordinal()];
        this.direction[CANGO.DOWN.ordinal()]  = this.direction[CANGO.RIGHT.ordinal()];
        this.direction[CANGO.RIGHT.ordinal()] = this.direction[CANGO.UP.ordinal()];
        this.direction[CANGO.UP.ordinal()]    = tmp;
    }

    /**
     * Otočí kámen o 90° doleva
     */
    public void turnLeft () {
        boolean tmp = this.direction[CANGO.LEFT.ordinal()];

        this.rotation = (this.rotation - 90) % 360;

        this.rotation = this.rotation < 0 ? 360 + this.rotation : this.rotation;

        this.direction[CANGO.LEFT.ordinal()]  = this.direction[CANGO.UP.ordinal()];
        this.direction[CANGO.UP.ordinal()]    = this.direction[CANGO.RIGHT.ordinal()];
        this.direction[CANGO.RIGHT.ordinal()] = this.direction[CANGO.DOWN.ordinal()];
        this.direction[CANGO.DOWN.ordinal()]  = tmp;
    }

    /**
     * Vrací který se nachází na políčku
     * @return vrací kámen, který je umístěný na políčku. Pokud na políčku žádný kámen není, vrací null
     */
    public Treasure getTreasure() {
        return this.treasure;
    }
    
    /**
     * Vloží hrací kámen na políčko
     * @param t hrací kámen, který budeme vkládat na políčko
     */
    public void putTreasure(Treasure t) {
        this.treasure = t;
    }
}
