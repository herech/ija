/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu CardPack, viz dokumentační komentář této třídy 
 */
 

package ija.game.treasure;
import java.util.*;

/**
 * Balíček karet
 * @author Jan Herec, Pavel Juhaňák
 * @see TreasureCard
 */
public class CardPack {
    private Stack<TreasureCard> stack = new Stack<TreasureCard>();

    private int actSize;

    /**
     * Konstruktor který inicializuje balíček karet a naplní jej nově vytvořenými kartami
     * @param initSize počáteční počet karet v balíčku
     */
    public CardPack (int initSize) {
        if (24 < initSize) { return; }

        this.actSize = initSize;

        for (int i = initSize ; i > 0 ; i--) {
            this.stack.push(new TreasureCard( Treasure.getTreasure(i) ));
        }
    }

    // konstruktor který se volá při načtení hry ze souboru
    public CardPack() {

    }

    /**
     * Metoda vrací kartu z vrchu balíčku
     * @return Metoda vrací kartu z vrchu balíčku
     */
    public TreasureCard popCard () {
        this.actSize--;

        return this.stack.pop();
    }

    /**
     * Metoda vrací aktuální počet karet v balíčku
     * @return Metoda vrací aktuální počet karet v balíčku
     */
    public int size () {
        return this.actSize;
    }

    /**
     * Nastaví aktuální velikost
     * @param actSize velikost
     */
    public void setActSize(int actSize) {
        this.actSize = actSize;
    }

    /**
     * Vrací aktuální velikost
     * @return this.actSize
     */
    public int getActSize() {
        return this.actSize;
    }

    /**
     * Metoda promíchá balíček karet
     */
    public void shuffle () {
        Collections.shuffle(this.stack);
    }

    /**
     * Vrací zásobník karet
     * @return this.stack
     */
    public Stack<TreasureCard> getPackageOfTReasureCards() {
        return this.stack;
    }

    /**
     * Přidá kartu s požadovaným pokladem na vrchol zásobníku
     * @param treasure_code kód požadovaného pokladu
     */
    public void pushCardToPack(int treasure_code) {
        this.stack.push(new TreasureCard( Treasure.getTreasure(treasure_code) ));
    }
}
