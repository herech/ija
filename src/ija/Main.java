/*
 * Projekt do předmětu IJA 2014/2015
 * Autoři: Jan Herec, xherec00
 *         Pavel Juhaňák, xjuhan01
 * Kódování: UTF-8
 * Popis: Soubor obsahuje jedinou třídu Main, viz dokumentační komentář této třídy
 */
 
package ija;

import ija.game.*;
import ija.gui.*;

/**
 * Třída Main reprezentuje vstupní bod programu, obsahuje jedinou metodu main() která načte zdroje a spustí GUI
 * @author Jan Herec, Pavel Juhaňák
 */

public class Main {

    public static void main(String argv[]) {
    
        // Obrázky se načtou při startu
        Game.loadImages();

        // Spustíme GUI
        GUI main_window = new GUI();
        main_window.setVisible(true);
    }
}
